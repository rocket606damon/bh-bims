<?php
/*
 * Template Name: Contact Us Page
 */
get_header();

?>

<!-- start site content -->
<div class="site_content">

  <?php
  if (asalah_post_option('asalah_onepage_scroll') != 'yes') {
      // asalah_page_title_holder();
  }
  ?>
  <?php
  remove_filter( 'the_content', 'wpautop' );

  remove_filter( 'the_excerpt', 'wpautop' );
  ?>
  <?php while (have_posts()) : the_post(); ?>
  	<div id="contact-us">
  		<div class="contact-banner-image">
  		</div>
  		<div id="contact-content" class="contact-content">
  			<div class="row">
  				<section class="main">
  					<div class="container contact-us">

  						<div class="row">
  							<div class="col-sm-6 col-md-5 contact-form">
                  <?php the_field('left_side_contact'); ?>
  							</div>
  							<div class="col-sm-6 col-md-7 contact-info">
                  <?php the_field('right_side_contact'); ?>
  							</div>
  						</div>

  					</div>
  				</section>
  			</div>
  		</div>
  	</div>
<?php endwhile; ?>
<script>
jQuery('body.page-template-contact-us').css("overflow-x", "initial");
</script>
</div>
<?php get_footer(); ?>
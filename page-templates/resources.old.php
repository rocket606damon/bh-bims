<?php
/*
 * Template Name: Resources Page
 */
get_header();
if (asalah_post_option('asalah_onepage_scroll') != 'yes') {
    // if(class_exists('RevSliderFront')) {
    //   asalah_rev_slider_wrapper();
    // }

}

?>
<!-- start site content -->
<?php
    if (asalah_post_option('asalah_onepage_scroll') != 'yes') {
        // asalah_page_title_holder();
    }
    ?>
    <?php
    	remove_filter( 'the_content', 'wpautop' );

    	remove_filter( 'the_excerpt', 'wpautop' );

    	remove_filter( 'acf_the_content', 'wpautop' );
    ?>
    <?php while (have_posts()) : the_post();
      $title = get_the_title();
      $title = preg_replace("/[^A-Za-z0-9 ]/", '', $title);
      $title = str_replace(" ", "-", $title);
      $title = strtolower($title);

    ?>
	<div id="hero-section" class="<?php echo $title; ?>-page">
		<div class="container text-center">
			<div class="row">
        <div class="col-sm-4 intro-block">
          <div class="page-header wp-caption">
            <?php if(has_post_thumbnail()):
              the_post_thumbnail();
              else:
            ?>
            <img class="intro-image" src="http://bh-bims.launchpaddev.com/wp-content/uploads/2017/02/Spaulding-Rehab.jpg" title="Spaulding-Rehab">
            <?php endif; ?>
          </div>
        </div>
        <div class="col-sm-8">
          <div class="page-header">
            <?php if(is_page('resources')): ?>
            <h2>Peer Support</h2>
            <?php else: ?>
            <h2><?php the_title(); ?></h2>
            <?php endif; ?>
          </div>
          <p class="top-text"><?php the_content(); ?></p>
        </div>
				<div class="col-sm-offset-2 col-sm-4 alignleft hero-content">
					<?php if(get_field('hero_heading')): ?><h2><?php the_field('hero_heading'); ?></h2><?php endif; ?>
					<?php if(get_field('hero_subheading')): ?><h4><?php the_field('hero_subheading'); ?></h4><?php endif; ?>
					<?php if(get_field('hero_secondary_text')): ?><p><?php the_field('hero_secondary_text'); ?></p><?php endif; ?>
					<?php if(get_field('hero_link')): ?><a href="<?php the_field('hero_link'); ?>" class="btn btn-primary">Read More</a><?php endif; ?>
				</div>
			</div>
		</div>
	</div>

  <div id="resources-nav" class="resources-nav">
    <div class="row">
      <section class="main">

        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <ul class="nav nav-tabs nav-justified">
                <li<?php if(is_page('resources') || is_page('peer-support')) echo ' class="active"'; ?>><a href="<?php bloginfo('url'); ?>/resources/peer-support/"><i class="icon icon-heart"></i>Peer Support</a></li>
                <li<?php if(is_page('factsheets')) echo ' class="active"'; ?>><a href="<?php bloginfo('url'); ?>/resources/factsheets/"><i class="fa fa-book fa-5x"></i>Factsheets</a></li>
                <li<?php if(is_page('videos')) echo ' class="active"'; ?>><a href="<?php bloginfo('url'); ?>/resources/videos/"><i class="fa fa-video-camera fa-5x"></i>Videos</a></li>
                <li<?php if(is_page('fire-safety-and-prevention')) echo ' class="active"'; ?>><a href="<?php bloginfo('url'); ?>/resources/fire-safety-and-prevention/"><i class="fa fa-fire-extinguisher fa-5x"></i>Fire Safety And Prevention</a></li>
                <li<?php if(is_page('disability-and-human-resources')) echo ' class="active"'; ?>><a href="<?php bloginfo('url'); ?>/resources/disability-and-human-resources/"><i class="icon icon-wheelchair"></i>Disability And Human Resources</a></li>
                <!-- <li<?php //if(is_page('other')) echo ' class="active"'; ?>><a href="<?php //bloginfo('url'); ?>/resources/other/"><i class="icon icon-other"></i>Other Resources</a></li> -->
              </ul>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>

		<?php if(is_page('resources') || is_page('peer-support')):
			require_once('peer-support.php');
		?>
		<?php else: ?>
	   <div id="tab-container" class="tab-container">
	    <div class="row">
      <section class="main">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                  <div class="tab-content">

                  <?php if(is_page('factsheets') && have_rows('factsheets', 298)): ?>
                    <div class="tab-pane active" id="factsheets">
                      <h2>Factsheets</h2>
                      <div class="resource-content">
                      <?php $i = 0; while (have_rows('factsheets', 298)) : the_row(); ?>
                        <h3><?php the_sub_field('section_title'); ?></h3>
                        <ul>
                        <?php $g = 0; while (have_rows('factsheet_list', 298)) : the_row(); ?>
                          <li>
                            <a href="<?php the_sub_field('link'); ?>" target="_blank"><i class="fa fa-book"></i> <?php the_sub_field('title'); ?></a>
                            <a data-toggle="collapse" data-target="#collapse<?php echo $i . "-" . $g ?>"><i class="fa fa-plus"></i></a>
                            <div id="collapse<?php echo $i . "-" . $g ?>" class="collapse"><div class="internal"><?php the_sub_field('description'); ?></div></div>
                          </li>
                        <?php $g++; endwhile; ?>
                        </ul>
                        <?php $i++; endwhile; ?>
                      </div>
                    </div>
                  <?php endif; ?>

                  <?php if(is_page('videos') && have_rows('videos', 298)): ?>
                    <div class="tab-pane active" id="videos">

                      <div class="resource-content">
                        <div class="row videos">

                        <div class="col-sm-12 select-btn">
	                        <div class="btn-group">
	                        	<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#" value="" postid="<?php echo $post->ID; ?>" metakey="video_select">Select a Category<span class="caret"></span></a>
	                        	<ul class="dropdown-menu">
	                        		<li><a href="#">Social &amp; Functional Recovery</a></li>
                              <li><a href="#">Instructional Therapy</a></li>
                              <li><a href="#">Research</a></li>
                              <li><a href="#">George's Top Ten</a></li>
	                        	</ul>
	                        </div>
                        </div>

                        <?php $i = 1; while (have_rows('videos', 298)) : the_row(); ?>
                        <?php
                          if(get_sub_field('video_type') == 'vimeo'):
                            $url = get_sub_field('vimeo');
                            $url = str_replace("http:", "", $url);
                            $url = str_replace("https:", "", $url);
                            $url = str_replace("www.", "", $url);
                            $url = str_replace("vimeo.com", "", $url);
                            $url = str_replace("/", "", $url);
                            $videoURL = '//player.vimeo.com/video/' . $url . '?title=0&amp;byline=0&amp;portrait=0';
                          else:
                            $url = get_sub_field('youtube');
                            $url = str_replace("http:", "", $url);
                            $url = str_replace("https:", "", $url);
                            $url = str_replace("www.", "", $url);
                            $url = str_replace("watch?v=", "", $url);
                            $url = str_replace("youtube.com", "", $url);
                            $url = str_replace("youtu.be", "", $url);
                            $url = str_replace("embed", "", $url);
                            $url = str_replace("/", "", $url);
                            $videoURL = '//www.youtube.com/embed/' . $url;
                          endif;
                        ?>

                          <div class="col-sm-12 video video-<?php echo $i; ?>">
                            <div class="col-sm-5">
                              <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="<?php echo $videoURL; ?>" allowfullscreen></iframe>
                              </div>
                            </div>
                            <div class="col-sm-7">
                              <h3><a href="<?php the_sub_field('video_url'); ?>" target="_blank"><?php the_sub_field('video_title'); ?></a></h3>
                              <?php if(get_sub_field('video_description')): ?><p class="video-description"><?php the_sub_field('video_description'); ?></p><?php endif; ?>
                            </div>
                          </div>

                        <?php $i++; endwhile; ?>

                          <div class="load-more col-sm-12">
                            <a href="#" class="load-more-videos">Load More Videos</a>
                          </div>

                        </div>
                      </div>
                    </div>

                  <?php endif; ?>

                  <?php if(is_page('fire-safety-and-prevention') && have_rows('fire_safety', 298)): ?>
                    <div class="tab-pane active" id="fire-safety">
                      <div class="resource-content">
                        <div class="row organizations">

                        	<?php $i = 1; while (have_rows('fire_safety', 298)) : the_row(); ?>
                          <div class="<?php echo ($i % 2 == 0) ? '' : 'col-md-offset-2'; ?> col-md-4">
                            <div class="organization fire-safety-<?php echo $i; ?>">
                              <img src="<?php the_sub_field('image'); ?>" class="img-circle" />
                              <div class="top-half">
                              <h4><?php the_sub_field('title'); ?></h4>
                              <p><?php the_sub_field('description', false, false); ?></p>
                              </div><!--End of Top Half-->
                              <div class="bottom-half">
                              <ul class="org-social clearfix">
                                <li class="twitter">
                                  <a href="<?php the_sub_field('twitter'); ?>" target="_blank">
                                    <i class="fa fa-twitter"></i>
                                  </a>
                                </li>
                              <!--<li class="email">
                                  <a href="<?php the_sub_field('email'); ?>" target="_blank">
                                    <i class="fa fa-envelope-o"></i>
                                  </a>
                                </li>-->
                                <li class="facebook">
                                  <a href="<?php the_sub_field('facebook'); ?>" target="_blank">
                                    <i class="fa fa-facebook"></i>
                                  </a>
                                </li>
                              </ul>
                              <a role="button" href="<?php the_sub_field('link'); ?>" class="btn btn-block btn-primary" target="_blank"><i class="fa fa-fire-extinguisher fa-2x"></i><?php the_sub_field('link_title'); ?></a>
                              </div><!--End of Bottom Half-->
                            </div>
                          </div>
                          <?php $i++; endwhile; ?>

                        </div>
                      </div>
                    </div>
                  <?php endif; ?>

                  <?php if(is_page('disability-and-human-resources') && have_rows('disability_resources', 298)): ?>
                  	<div class="tab-pane active" id="disability-resources">
                      <div class="resource-content">
                        <div class="row organizations">

                          <?php $i = 1; while (have_rows('disability_resources', 298)) : the_row(); ?>

                          <?php
                            $modal = get_sub_field('button_title');
                            $modal = preg_replace("/[^A-Za-z0-9 ]/", '', $modal);
                            $modal = str_replace(" ", "-", $modal);
                            $modal = strtolower($modal);

                            //echo $modal;
                          ?>

                          <div class="col-sm-6 col-md-4">
                            <div class="profile services-<?php echo $i; ?>">
                              <img src="<?php the_sub_field('image'); ?>" class="img-circle" />
                              <!-- <p><?php //the_sub_field('title'); ?></p> -->
                              <a role="button" href="#" class="btn btn-block btn-primary" data-toggle="modal" data-target="#<?php echo $modal; ?>"><?php the_sub_field('button_title'); ?></a>
                            </div>
                          </div>

                          <div class="modal fade" id="<?php echo $modal; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                              <div class="modal-content group">

                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                  <h4 class="modal-title" id="myModalLabel"><?php the_sub_field('modal_title'); ?></h4>
                                </div>

                                <div class="modal-body">
                                  <?php the_sub_field('modal_content'); ?>
                                </div><!-- Modal-Body -->

                              </div><!-- Modal-Content Group -->
                            </div>
                          </div>

                        <?php endwhile; ?>

                        </div>
                      </div>
                    </div>
                	<?php endif; ?>
                  </div>
                </div>
            </div>
        </div>

      </section><!-- /.main -->
    </div><!-- /.content -->
  </div><!-- /.wrap -->
  <?php endif; ?>

  <?php endwhile; ?>

</div>
<?php get_footer(); ?>
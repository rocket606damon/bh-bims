<?php
/*
 * Template Name: Research Page
 */
get_header();
if (asalah_post_option('asalah_onepage_scroll') != 'yes') {
    // if(class_exists('RevSliderFront')) {
    //   asalah_rev_slider_wrapper();
    // }

}

?>
<style>
.nav-tabs > li.active:last-child > a {
    border-right-color: #c70b2c;!important;
    border-right-width: 0!important;
}
</style>
<!-- start site content -->
<div class="site_content">

  <?php
  if (asalah_post_option('asalah_onepage_scroll') != 'yes') {
      // asalah_page_title_holder();
  }
  ?>
  <?php
  	remove_filter( 'the_content', 'wpautop' );

  	remove_filter( 'the_excerpt', 'wpautop' );

  	remove_filter('acf_the_content', 'wpautop');
  ?>
  <?php while (have_posts()) : the_post(); ?>

  <div id="hero-research">
  </div>

	<div id="tabs">
		<div id="research-nav" class="research-nav">
			<div class="row">
				<section class="main">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<ul class="nav nav-tabs nav-justified">
									<li id="li-tabs-1" class="li-clicker"><a href="#tabs-1" class="tab-clicker" id="tab-clicker1"><i class="icon icon-libre"></i>Libre Profile</a></li>
									<li id="li-tabs-2" class="li-clicker"><a href="#tabs-2" class="tab-clicker" id="tab-clicker2"><i class="icon icon-database"></i>Longitudinal Database</a></li>
									<li id="li-tabs-3" class="li-clicker"><a href="#tabs-3" class="tab-clicker" id="tab-clicker3"><i class="icon icon-publications"></i>Publications</a></li>
									<li id="li-tabs-4" class="li-clicker"><a href="#tabs-4" class="tab-clicker" id="tab-clicker4"><i class="icon icon-other"></i>Other</a></li>
								</ul>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>

		<div id="tab-container" class="tab-container">
			<div class="row">
				<section class="main">
					<div class="container">
						<div class="row">
							<div class="col-sm-12" id="something">
								<div class="tab-content">
        <style>
        .libre-content {
          margin-bottom: 0px;
        }
        .libre-content h3 {
          text-align: center;
          width: 100%;
          margin: 0 auto 30px;
          font-size: 20px;
        }
        .libre-inner p, .libre-inner ul li {
          font-size: 14px;
        }
        .libre-other {
          margin: 20px 0 60px;
        }
        .libre-other .profile h4 {
          text-align: center;
          font-size: 24px;
        }
        .libre-other .profile .research-other-content p {
          font-size: 14px;
          text-align: center;
        }
        </style> 
        <div id="tabs-1">
          <div class="tab-pane active" id="libre-profile">
            <div class="research-content">
              <div class="row">
                <div class="col-sm-12 col-md-12 libre-content clearfix">
                  <?php the_field('libre_content', 409); ?>
                </div>
                  <div class="col-sm-12 col-md-12 libre-other clearfix">
                  <?php if(have_rows('libre_other', 409)): $i = 0;
                  while (have_rows('libre_other', 409)) : $i++; the_row(); 

                  ?>
                  <?php
                    $modal = get_sub_field('modal_title');
                    $modal = preg_replace("/[^A-Za-z0-9 ]/", '', $modal);
                    $modal = str_replace(" ", "-", $modal);
                    $modal = strtolower($modal) . "_a";
                    
                    //echo $modal;
                  ?>
                  <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="profile research-other-1">
                      <h4><?php the_sub_field('title'); ?></h4>
                      <div class="research-other-content" data-mh="research-other">
                        <?php the_sub_field('description_content'); ?>
                      </div>
                      <a role="button" href="#" class="btn btn-block btn-primary" data-toggle="modal" data-target="#<?php echo $modal; ?>">Learn More</a>
                    </div>
                  </div>
                  <div class="modal fade" id="<?php echo $modal; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content group">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title" id="myModalLabel"><?php the_sub_field('modal_title'); ?></h4>
                        </div>

                        <div class="modal-body">
                          <?php the_sub_field('modal_content'); ?>
                        </div><!-- Modal-Body -->

                      </div><!-- Modal-Content Group -->
                    </div>
                  </div>
                  <?php endwhile; endif; ?>
                  </div><!--end of Libre other-->
                  <div class="col-sm-12 col-md-12 libre-bottom" style="display: none;">
                    <?php the_field('libre_bottom', 409); ?>
                  </div>

                  <div class="col-sm-12 col-md-12 libre-video clearfix">
                    <div class="col-sm-5">
                      <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="<?php the_field('libre_embed', 409); ?>" allowfullscreen></iframe>
                      </div>
                    </div>
                    <div class="col-sm-7">
                      <h3><a href="<?php the_field('libre_vlink', 409); ?>" target="_blank"><?php the_field('libre_video_title', 409); ?></a></h3>
                      <?php the_field('libre_video_description', 409); ?>
                    </div>
                  </div>
                </div>
                
            </div>
          </div>
        </div><!--End of Tab-1-->

        <div id="tabs-2">
          <div class="tab-pane active" id="longitudinal-database">
            <div class="research-content">
              <div class="row">
                <div class="col-sm-12 col-md-12 database-top">
                  <h3><?php the_field('longitudinal_header', 409); ?></h3>
                </div>
              	<div class="col-sm-12 col-md-12">
                  <div class="database-content">
                		<?php the_field('longitudinal_content', 409); ?>
                    <div class="database-img">
                      <img src="<?php the_field('longitudinal_img', 409); ?>" class="responsive" />
                      <figcaption>
                        <?php the_field('longitudinal_figcaption', 409); ?>
                      </figcaption>
                    </div>
                		<?php the_field('longitudinal_bottom', 409); ?>
                  </div>
              		<div class="database-bottom">
              			<p><?php the_field('facts_figures_title', 409); ?></p>
                    <div class="row">
                			<div class="col-xs-6 col-sm-6 col-md-6">
                				<a href="<?php the_field('facts_figures_adults', 409); ?>" class="btn btn-large btn-primary database-custom" type="button" target="_blank">Click Here for <br><span class="btn-bold">Adults</span></a>
                			</div>
                			<div class="col-xs-6 col-sm-6 col-md-6">
                				<a href="<?php the_field('facts_figures_children', 409); ?>" class="btn btn-large btn-primary database-custom" type="button" target="_blank">Click Here for <br><span class="btn-bold">Children</span></a>
                			</div>
                    </div>
              		</div>
              	</div>
              </div>
            </div>
          </div>
        </div><!--End of Tabs-2-->

        <div id="tabs-3">
          <div class="tab-pane active" id="publications">
  					<div class="container publication-container" style="background-color: #ffffff;">
  						<div class="row" style="background-color: #ffffff;">
  			      <?php include(dirname(__FILE__)."/../lib/publications.html"); ?>
  						</div>
  					</div>
          </div>
        </div><!--End of Tab-3-->

        <div id="tabs-4">
          <div class="tab-pane active" id="research-other">
            <div class="research-content">
              <div class="row organizations">

              	<?php if(have_rows('research_other', 409)): $i = 0;
          			while (have_rows('research_other', 409)) : $i++; the_row();
          			?>
                <?php
                  $modal = get_sub_field('modal_title');
                  $modal = preg_replace("/[^A-Za-z0-9 ]/", '', $modal);
                  $modal = str_replace(" ", "-", $modal);
                  $modal = strtolower($modal);

                  //echo $modal;
                ?>
                <div class="col-sm-6 col-md-4">
                  <div class="profile research-other-1">
                    <h4><?php the_sub_field('title'); ?></h4>
                    <div class="research-other-content" data-mh="research-other">
                      <?php the_sub_field('description_content'); ?>
                    </div>
                    <a role="button" href="#" class="btn btn-block btn-primary" data-toggle="modal" data-target="#<?php echo $modal; ?>">Learn More</a>
                  </div>
                </div>
                <div class="modal fade" id="<?php echo $modal; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-<?php echo $i; ?>" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content group">

                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel-<?php echo $i; ?>"><?php the_sub_field('modal_title'); ?></h4>
                      </div>

                      <div class="modal-body">
                        <?php the_sub_field('modal_content'); ?>
                      </div><!-- Modal-Body -->

                    </div><!-- Modal-Content Group -->
                  </div>
                </div>
              	<?php endwhile; endif; ?>

              </div>
            </div>
          </div>
        </div><!--End of Tab-4-->

              </div>
            </div>
          </div>
        </div>

			</section>
		</div>
	</div><!--End of tab-container-->
</div><!--End of Tabs-->

  <?php endwhile; ?>

</div>
<?php get_footer(); ?>
<?php
/*
 * Template Name: Resources Page
 */
get_header();
if (asalah_post_option('asalah_onepage_scroll') != 'yes') {
    // if(class_exists('RevSliderFront')) {
    //   asalah_rev_slider_wrapper();
    // }

}

?>
<!-- start site content -->
<?php
    if (asalah_post_option('asalah_onepage_scroll') != 'yes') {
        // asalah_page_title_holder();
    }
    ?>
    <?php
    	remove_filter( 'the_content', 'wpautop' );
    	remove_filter( 'the_excerpt', 'wpautop' );
    	remove_filter( 'acf_the_content', 'wpautop' );
    ?>
    <?php while (have_posts()) : the_post();
      $title = get_the_title();
      $title = preg_replace("/[^A-Za-z0-9 ]/", '', $title);
      $title = str_replace(" ", "-", $title);
      $title = strtolower($title);
    ?>
	<div id="hero-section" class="<?php echo $title; ?>-page">
		<div class="container text-center">
			<div class="row">
        <div class="col-sm-4 intro-block">
          <div class="page-header wp-caption">
            <img id="page-header-image" class="intro-image" src="" title="Spaulding-Rehab" alt="spaulding-rehab" >
          </div>
        </div>
        <div class="col-sm-8">
          <div class="page-header">
			<h2 id="page-header-title"></h2>
          </div>
          <p id="page-header-text" class="top-text"></p>
        </div>
				<div class="col-sm-offset-2 col-sm-4 alignleft hero-content">
					<?php if(get_field('hero_heading')): ?><h2><?php the_field('hero_heading'); ?></h2><?php endif; ?>
					<?php if(get_field('hero_subheading')): ?><h4><?php the_field('hero_subheading'); ?></h4><?php endif; ?>
					<?php if(get_field('hero_secondary_text')): ?><p><?php the_field('hero_secondary_text'); ?></p><?php endif; ?>
					<?php if(get_field('hero_link')): ?><a href="<?php the_field('hero_link'); ?>" class="btn btn-primary">Read More</a><?php endif; ?>
				</div>
			</div>
		</div>
	</div>

  <div id="tabs">
    <div id="resources-nav" class="resources-nav">
      <div class="row">
        <section class="main">

          <div class="container">
            <div class="row">
              <div class="col-sm-12">
        				<ul class="nav nav-tabs nav-justified">
        					<li id="li-tabs-1" class="li-clicker"><a href="#tabs-1" class="tab-clicker" id="tab-clicker1"><i class="icon icon-heart"></i>Peer Support</a></li>
        					<li id="li-tabs-2" class="li-clicker"><a href="#tabs-2" class="tab-clicker" id="tab-clicker2"><i class="fa fa-book fa-5x"></i>Factsheets</a></li>
        					<li id="li-tabs-3" class="li-clicker"><a href="#tabs-3" class="tab-clicker" id="tab-clicker3"><i class="fa fa-video-camera fa-5x"></i>Videos</a></li>
        					<li id="li-tabs-4" class="li-clicker"><a href="#tabs-4" class="tab-clicker" id="tab-clicker4"><i class="fa fa-fire-extinguisher fa-5x"></i>Fire Safety And Prevention</a></li>
        					<li id="li-tabs-5" class="li-clicker"><a href="#tabs-5" class="tab-clicker" id="tab-clicker5"><i class="icon icon-wheelchair"></i>Disability And Human Resources</a></li>
        				</ul>
  				    </div>
            </div>
          </div>

        </section>
      </div>
    </div><!--End of Resources-Nav-->

	<div id="tab-container" class="tab-container">
		<div class="row">
			<section class="main">
          <div class="container">
            <div class="row">
              <div class="col-sm-12">
                <div class="tab-content">

					<!-- Start of Tab-1-->
          			<div id="tabs-1">
					  <div class="tab-pane" id="peer-support">
						<div class="resources-content" id="peer-top">
							<div class="row organizations">
								<?php $i = 1; while (have_rows('peer_support_top', 298)) : the_row();
									$peer_top = get_sub_field('image');
									$alt = $peer_top['alt'];
									$size = 'full_size';
									$thumb = $peer_top['sizes'][$size];
								?>
								<div class="col-md-4">
									<div class="organization peer-top-<?php echo $i; ?>"  data-mh="peer-top">
										<img src="<?php echo $thumb; ?>" class="img-responsive" alt="<?php echo $alt; ?>" />
										<h4><?php the_sub_field('title'); ?></h4>
										<?php the_sub_field('description', false, false); ?>
									</div>
								</div>
								<?php $i++; endwhile; ?>
							</div>
						</div>

						<div class="resource-content" id="peer-middle">
							<div class="row organizations">
								<?php $i = 1; while (have_rows('peer_support', 298)) : the_row();
									$organization = get_sub_field('image');
									$alt = $organization['alt'];
									$size = 'full_size';
									$thumb = $organization['sizes'][$size];
								?>
								<div class="<?php echo ($i % 2 == 0) ? '' : 'col-md-offset-2'; ?> col-md-4">
									<div class="organization peer-support-<?php echo $i; ?>">
										<img src="<?php echo $thumb; ?>" class="img-circle" alt="<?php echo $alt; ?>" />
										<div class="top-half">
											<h4><?php the_sub_field('title'); ?></h4>
											<p><?php the_sub_field('description', false, false); ?></p>
										</div>
										<!--End of Top Half-->
										<div class="bottom-half">
										<!-- <ul class="org-social clearfix">
											<li class="twitter">
												<a href="<?php the_sub_field('twitter'); ?>" target="_blank">
													<i class="fa fa-twitter"></i>
												</a>
											</li>
											<li class="email">
												<a href="<?php the_sub_field('email'); ?>" target="_blank">
												  <i class="fa fa-envelope-o"></i>
												</a>
											</li>
											<li class="facebook">
												<a href="<?php the_sub_field('facebook'); ?>" target="_blank">
												  <i class="fa fa-facebook"></i>
												</a>
											</li>
										</ul> -->
										<a role="button" href="<?php the_sub_field('link'); ?>" class="btn btn-block btn-primary" target="_blank">
											<i class="icon icon-heart"></i><?php the_sub_field('link_title'); ?>
										</a>
									</div><!--End of Bottom Half-->
								</div>
							</div>
							<?php $i++; endwhile; ?>
						</div>
					</div>
					<div class="resource-content" id="peer-bottom">
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<h1>Boston Area Peer Supporters</h1>
								</div>
							</div>
						</div>
						<div class="row profiles">
							<?php $i = 1; while (have_rows('peer_supporters', 298)) : the_row(); ?>
							<?php
								$modal = get_sub_field('name');
								$modal = preg_replace("/[^A-Za-z0-9 ]/", '', $modal);
								$modal = str_replace(" ", "-", $modal);
								$modal = strtolower($modal);

								$peer_supporters = get_sub_field('image');
								$alt = $peer_supporters['alt'];
								$size = 'medium';
								$thumb = $peer_supporters['sizes'][$size];

								//echo $modal;
						   ?>
							<div class="col-sm-4">
								<div class="profile peer-team-<?php echo $i; ?>">
									<img src="<?php echo $thumb; ?>" class="img-circle" alt="<?php echo $alt; ?>" />
									<h4><?php the_sub_field('name'); ?></h4>
									<a role="button" href="#" class="btn btn-block btn-primary" data-toggle="modal" data-target="#<?php echo $modal; ?>">View Bio</a>
								</div>
							</div>
							<div class="modal fade" id="<?php echo $modal; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg">
									<div class="modal-content group">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
											<h4 class="modal-title" id="myModalLabel"><?php the_sub_field('name'); ?></h4>
										</div>
										<div class="modal-body">
											<?php the_sub_field('bio'); ?>
										</div><!-- Modal-Body -->
									</div><!-- Modal-Content Group -->
								</div>
							</div>
							<?php $i++; endwhile; ?>
						</div>
					</div>
				</div>
			</div>
			<!-- End of Tab-1-->

			<!-- Start of Tab-2-->
			<div id="tabs-2">
				<div class="tab-pane" id="factsheets">
					<!-- <h2>Factsheets</h2> -->
					<div class="resource-content">
					<?php $i = 0; while (have_rows('factsheets', 298)) : the_row(); ?>
						<h3><?php the_sub_field('section_title'); ?></h3>
						<ul>
							<?php $g = 0; while (have_rows('factsheet_list', 298)) : the_row(); ?>
							<li>
								<a href="<?php the_sub_field('link'); ?>" target="_blank"><i class="fa fa-book"></i> <?php the_sub_field('title'); ?></a>
								<a data-toggle="collapse" data-target="#collapse<?php echo $i . "-" . $g ?>"><i class="fa fa-plus"></i></a>
								<div id="collapse<?php echo $i . "-" . $g ?>" class="collapse"><div class="internal"><?php the_sub_field('description'); ?></div></div>
							</li>
							<?php $g++; endwhile; ?>
						</ul>
						<?php $i++; endwhile; ?>
					</div>
				</div>
			</div><!-- End of Tab-2-->

			 <div id="tabs-3">
				<div class="tab-pane active" id="videos">
					<div class="resource-content">
						<div class="row videos">
							<div class="col-sm-12 select-btn">
								<div class="video-toggle-select btn-group">
									<a id="bh-top-nav" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#" value="" postid="<?php echo $post->ID; ?>" metakey="video_select">Select a Category<span class="caret"></span>
									</a>
									<ul id="video-dropdown-menu" class="dropdown-menu">
										<li><a href="#">Social &amp; Functional Recovery</a></li>
										<li><a href="#">Instructional Therapy</a></li>
										<li><a href="#">Research</a></li>
										<li><a href="#">George's Top Ten</a></li>
									</ul>
								</div>
							</div>
							<div class="video-list" style="min-height:600px"></div>
							<div class="load-more col-sm-12">
								<a href="#" class="load-more-videos">Load More Videos</a>
							</div>
						</div>
					</div>
				</div>
			</div><!--End of Tabs-3-->

              <div id="tabs-4">
                <div class="tab-pane active" id="fire-safety">
                  <div class="resource-content">
                    <div class="row organizations">

                    	<?php $i = 1; while (have_rows('fire_safety', 298)) : the_row();

                        $safety = get_sub_field('image');
                        $alt = $safety['alt'];
                        $size = 'full_size';
                        $thumb = $safety['sizes'][$size];
                      ?>
                      <div class="<?php //echo ($i % 2 == 0) ? '' : 'col-md-offset-2'; ?> col-md-4">
                        <div class="organization fire-safety-<?php echo $i; ?>">
                          <img src="<?php echo $thumb; ?>" class="img-circle" alt="<?php echo $alt; ?>" />
                          <div class="top-half">
                            <h4><?php the_sub_field('title'); ?></h4>
                            <p><?php the_sub_field('description', false, false); ?></p>
                          </div><!--End of Top Half-->
                          <div class="bottom-half">
                          <ul class="org-social clearfix">
                            <li class="twitter">
                              <a href="<?php the_sub_field('twitter'); ?>" target="_blank">
                                <i class="fa fa-twitter"></i>
                              </a>
                            </li>
                          <!--<li class="email">
                              <a href="<?php the_sub_field('email'); ?>" target="_blank">
                                <i class="fa fa-envelope-o"></i>
                              </a>
                            </li>-->
                            <li class="facebook">
                              <a href="<?php the_sub_field('facebook'); ?>" target="_blank">
                                <i class="fa fa-facebook"></i>
                              </a>
                            </li>
                          </ul>
                          <a role="button" href="<?php the_sub_field('link'); ?>" class="btn btn-block btn-primary" target="_blank"><i class="fa fa-fire-extinguisher fa-2x"></i><?php the_sub_field('link_title'); ?></a>
                          </div><!--End of Bottom Half-->
                        </div>
                      </div>
                      <?php $i++; endwhile; ?>

                    </div>
                  </div>
                </div>
              </div><!--End of Tabs-4-->

              <div id="tabs-5">
				<div class="tab-pane active" id="disability-resources">
                  <div class="resource-content">
                    <div class="row organizations">
						<?php $i = 1; while (have_rows('disability_resources', 298)) : the_row(); ?>
						<?php
						$modal = get_sub_field('button_title');
						$modal = preg_replace("/[^A-Za-z0-9 ]/", '', $modal);
						$modal = str_replace(" ", "-", $modal);
						$modal = strtolower($modal);
						//echo $modal;

						$disability = get_sub_field('image');
						$alt = $disability['alt'];
						$size = 'full_size';
						$thumb = $disability['sizes'][$size];
						?>
				<div class="col-sm-6 col-md-4">
					<div class="profile services-<?php echo $i; ?>">
						<img src="<?php echo $thumb; ?>" class="img-circle" alt="<?php echo $alt; ?>" />
						<!-- <p><?php //the_sub_field('title'); ?></p> -->
						<a role="button" href="#" class="btn btn-block btn-primary" data-toggle="modal" data-target="#<?php echo $modal; ?>"><?php the_sub_field('button_title'); ?></a>
					</div>
				</div>

				<div class="modal fade" id="<?php echo $modal; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content group">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								<h4 class="modal-title" id="myModalLabel"><?php the_sub_field('modal_title'); ?></h4>
							</div>
							<div class="modal-body">
							<?php the_sub_field('modal_content'); ?>
							</div><!-- Modal-Body -->
                          </div><!-- Modal-Content Group -->
                        </div>
                      </div>
                    <?php endwhile; ?>
                    </div>
                  </div>
                </div>
          		</div><!--End of Tabs-5-->

              </div>
            </div>
          </div>
        </div>
      </section><!-- /.main -->
    </div><!-- /.content -->
  </div><!-- /.wrap -->

  <?php endwhile; ?>

</div>
<?php get_footer(); ?>
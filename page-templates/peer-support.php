
	<?php if(is_page('resources') || is_page('peer-support') && have_rows('peer_support', 298)): ?>
	  <div class="tab-pane active" id="peer-support">

	  	<div class="resources-content" id="peer-top">
	  		<div class="row organizations">
	  			<?php $i = 1; while (have_rows('peer_support_top', 298)) : the_row(); 

	  				$peer_top = get_sub_field('image');
	  				$alt = $peer_top['alt'];
	  				$size = 'full_size';
	  				$thumb = $peer_top['sizes'][$size];
	  			?>

	  			<div class="col-md-4">
	  				<div class="organization peer-top-<?php echo $i; ?>"  data-mh="peer-top">
	  					<img src="<?php echo $thumb; ?>" class="img-responsive" alt="<?php echo $alt; ?>" />
	  					<h4><?php the_sub_field('title'); ?></h4>
	  					<?php the_sub_field('description', false, false); ?>
	  				</div>
	  			</div>

					<?php $i++; endwhile; ?>
	  		</div>
	  	</div>

	    <div class="resource-content" id="peer-middle">
	      <div class="row organizations">

	      <?php $i = 1; while (have_rows('peer_support', 298)) : the_row(); 

	      	$organization = get_sub_field('image');
	      	$alt = $organization['alt'];
	      	$size = 'full_size';
	      	$thumb = $organization['sizes'][$size];
	      ?>
	        <div class="<?php echo ($i % 2 == 0) ? '' : 'col-md-offset-2'; ?> col-md-4">
	          <div class="organization peer-support-<?php echo $i; ?>">
	            <img src="<?php echo $thumb; ?>" class="img-circle" alt="<?php echo $alt; ?>" />
	            <div class="top-half">
	            <h4><?php the_sub_field('title'); ?></h4>
	            <p><?php the_sub_field('description', false, false); ?></p>
	            </div><!--End of Top Half-->
	            <div class="bottom-half">
	            <!-- <ul class="org-social clearfix">
	              <li class="twitter">
	                <a href="<?php the_sub_field('twitter'); ?>" target="_blank">
	                  <i class="fa fa-twitter"></i>
	                </a>
	              </li>
	              <li class="email">
	                <a href="<?php the_sub_field('email'); ?>" target="_blank">
	                  <i class="fa fa-envelope-o"></i>
	                </a>
	              </li>
	              <li class="facebook">
	                <a href="<?php the_sub_field('facebook'); ?>" target="_blank">
	                  <i class="fa fa-facebook"></i>
	                </a>
	              </li>
	            </ul> -->
	            <a role="button" href="<?php the_sub_field('link'); ?>" class="btn btn-block btn-primary" target="_blank"><i class="icon icon-heart"></i><?php the_sub_field('link_title'); ?></a>
	            </div><!--End of Bottom Half-->
	          </div>
	        </div>
	        <?php $i++; endwhile; ?>

	      </div>
	    </div>

	    <div class="resource-content" id="peer-bottom">
	    	<div class="row">
          <div class="col-sm-12">
            <div class="page-header">
              <h1>Boston Area Peer Supporters</h1>
            </div>
          </div>
        </div>
	    	<div class="row profiles">
	    		<?php $i = 1; while (have_rows('peer_supporters', 298)) : the_row(); ?>

	    		<?php
            $modal = get_sub_field('name');
            $modal = preg_replace("/[^A-Za-z0-9 ]/", '', $modal);
            $modal = str_replace(" ", "-", $modal);
            $modal = strtolower($modal);

            $peer_supporters = get_sub_field('image');
            $alt = $peer_supporters['alt'];
            $size = 'medium';
            $thumb = $peer_supporters['sizes'][$size];
            
            //echo $modal;
           ?>
	    		<div class="col-sm-4">
	    			<div class="profile peer-team-<?php echo $i; ?>">
		    			<img src="<?php echo $thumb; ?>" class="img-circle" alt="<?php echo $alt; ?>" />
		    			<h4><?php the_sub_field('name'); ?></h4>
		    			<a role="button" href="#" class="btn btn-block btn-primary" data-toggle="modal" data-target="#<?php echo $modal; ?>">View Bio</a>
	    			</div>
	    		</div>
	    		<div class="modal fade" id="<?php echo $modal; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content group">

                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  <h4 class="modal-title" id="myModalLabel"><?php the_sub_field('name'); ?></h4>
                </div>
                
                <div class="modal-body">
                  <?php the_sub_field('bio'); ?>  
                </div><!-- Modal-Body -->
                
              </div><!-- Modal-Content Group -->
            </div>
          </div>
	    		<?php $i++; endwhile; ?>
	    	</div>
	    </div>

	  </div>
	<?php endif; ?>                      
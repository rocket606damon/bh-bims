<?php
/*
 * Template Name: About Us Page
 */
get_header();
if (asalah_post_option('asalah_onepage_scroll') != 'yes') {
    // if(class_exists('RevSliderFront')) {
    //   asalah_rev_slider_wrapper();
    // }

}

?>
<!-- start site content -->
<div class="site_content">

    <?php
    if (asalah_post_option('asalah_onepage_scroll') != 'yes') {
        // asalah_page_title_holder();
    }
    ?>
    <?php
    remove_filter( 'the_content', 'wpautop' );

    remove_filter( 'the_excerpt', 'wpautop' );
    ?>
    <?php while (have_posts()) : the_post(); ?>
    <div id="about-us">
      <section class="main" role="overview">
        <div class="row">
          <div class="container about-us">
            <div class="row">
                <div class="col-sm-4 intro-block">
                  <div class="page-header wp-caption">
                    <?php
                      //vars
                      $image = get_field('intro_image');
                      $title = $image['title'];
                      $caption = $image['caption'];

                      // medium
                      $size = 'large';
                      $large = $image['sizes'][$size];
                    ?>
                    <img class="intro-image" src="<?php echo $large; ?>" title="<?php echo $title; ?>" alt="<?php echo $title; ?>"/>
                    <!-- <p class="wp-caption-text"><?php //echo $caption; ?></p> -->
                    <!-- <i class="fa fa-info-circle fa-5x"></i> -->
                  </div>
                  <ul class="clearfix about-nav">
                    <li><i class="icon icon-about-us-research"></i><a href="<?php bloginfo('url'); ?>/research/">Research</a></li>
                    <li><i class="icon icon-about-resources"></i><a href="<?php bloginfo('url'); ?>/resources/">Education</a></li>
                    <li><i class="icon icon-about-us-medical-center"></i><a href="#medical-centers">Clinical Resources</a></li>
                  </ul>
                </div>
                <div class="col-sm-8">
                    <div class="page-header">
                      <h2><?php the_field('intro_title'); ?></h2>
                    </div>
                    <?php the_field('intro_content'); ?>
                    <div class="page-header">
                      <h2><?php the_field('modelsys_intro'); ?></h2>
                    </div>
                    <?php the_field('modelsys_content'); ?>
                </div>
            </div>
          </div>
        </div>
      </section><!-- /.main -->
    </div>

    <div class="meet-the-team medical-team" id="medical-team">
      <section class="main" role="bios">
        <div class="row">
          <div class="container about-us">
              <div class="row">
                  <div class="col-sm-12">
                      <div class="page-header">
                          <h1><?php the_field('medical_team_title'); ?></h1>
                      </div>
                  </div>
              </div>
              <div class="row profiles">
                  <?php if(have_rows('medical_team_bio')): $i = 0;
                  while (have_rows('medical_team_bio')) : $i++; the_row();

                  ?>
                  <?php
                      $modal = get_sub_field('name');
                      $modal = preg_replace("/[^A-Za-z0-9 ]/", '', $modal);
                      $modal = str_replace(" ", "-", $modal);
                      $modal = strtolower($modal);

                      $photo = get_sub_field('photo');
                      $alt = $photo['alt'];
                      $size = 'medium';
                      $thumb = $photo['sizes'][$size];

                      //echo $modal;
                  ?>

                  <div class="col-sm-3">
                      <div class="profile medical-team-<?php echo $i; ?>">
                          <img src="<?php echo $thumb; ?>" class="img-circle" alt="<?php echo $alt; ?>" />
                          <h4><?php the_sub_field('name'); ?></h4>
                          <p><?php the_sub_field('title'); ?></p>
                          <a role="button" href="#" class="btn btn-block btn-primary" data-toggle="modal" data-target="#<?php echo $modal; ?>">View Biography</a>
                      </div>
                  </div>
                  <div class="modal fade" id="<?php echo $modal; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content group">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title" id="myModalLabel"><?php the_sub_field('name'); ?> - <?php the_sub_field('title'); ?></h4>
                        </div>

                        <div class="modal-body">
                          <div class="thumbnail alignleft">
                              <img class="img-rounded" src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>">
                              <div class="caption">
                                  <!-- <div class="social">
                                      <?php if(get_sub_field('email')): ?><a href="mailto:<?php the_sub_field('email'); ?>" class="email" target="_blank"><span class="fa-stack fa-sm"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-envelope fa-stack-1x fa-inverse"></i></span></a><?php endif; ?>
                                      <?php if(get_sub_field('linkedin')): ?><a href="<?php the_sub_field('linkedin'); ?>" class="linkedin" target="_blank"><span class="fa-stack fa-sm"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-linkedin fa-stack-1x fa-inverse"></i></span></a><?php endif; ?>
                                  </div> -->
                              </div>
                          </div>
                          <?php the_sub_field('bio'); ?>
                        </div>

                      </div>
                    </div>
                  </div>
                  <?php endwhile;endif; ?>
              </div>

          </div>
        </div>
      </section>
    </div>

    <div class="meet-the-team research-team" id="research-team">
      <section class="main" role="bios">
        <div class="row">
          <div class="container about-us">
              <div class="row">
                  <div class="col-sm-12">
                      <div class="page-header">
                          <h1><?php the_field('research_team_title'); ?></h1>
                      </div>
                  </div>
              </div>
              <div class="row profiles">
                  <?php if(have_rows('research_team_bio')): $i = 0;
                  while (have_rows('research_team_bio')) : $i++; the_row();

                  ?>
                  <?php
                      $modal = get_sub_field('name');
                      $modal = preg_replace("/[^A-Za-z0-9 ]/", '', $modal);
                      $modal = str_replace(" ", "-", $modal);
                      $modal = strtolower($modal);

                      $research = get_sub_field('photo');
                      $alt = $research['alt'];
                      $size = 'medium';
                      $thumb = $research['sizes'][$size];

                      //echo $modal;
                  ?>

                  <div class="col-sm-3">
                      <div class="profile research-team-<?php echo $i; ?>">
                          <img src="<?php echo $thumb; ?>" class="img-circle" alt="<?php echo $alt; ?>" />
                          <h4><?php the_sub_field('name'); ?></h4>
                          <p><?php the_sub_field('title'); ?></p>
                          <a role="button" href="#" class="btn btn-block btn-primary" data-toggle="modal" data-target="#<?php echo $modal; ?>">View Biography</a>
                      </div>
                  </div>
                  <div class="modal fade" id="<?php echo $modal; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content group">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title" id="myModalLabel"><?php the_sub_field('name'); ?> - <?php the_sub_field('title'); ?></h4>
                        </div>

                        <div class="modal-body">
                          <div class="thumbnail alignleft">
                              <img class="img-rounded" src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>">
                              <div class="caption">
                                  <!-- <div class="social">
                                      <?php if(get_sub_field('email')): ?><a href="mailto:<?php the_sub_field('email'); ?>" class="email" target="_blank"><span class="fa-stack fa-sm"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-envelope fa-stack-1x fa-inverse"></i></span></a><?php endif; ?>
                                      <?php if(get_sub_field('linkedin')): ?><a href="<?php the_sub_field('linkedin'); ?>" class="linkedin" target="_blank"><span class="fa-stack fa-sm"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-linkedin fa-stack-1x fa-inverse"></i></span></a><?php endif; ?>
                                  </div> -->
                              </div>
                          </div>
                          <?php the_sub_field('bio'); ?>
                        </div>

                      </div>
                    </div>
                  </div>
                  <?php endwhile;endif; ?>
              </div>

          </div>
        </div>
      </section>
    </div>

    <div class="meet-the-team contributor-team" id="former-staff-members">
      <section class="main" role="bios">
        <div class="row">
          <div class="container about-us">
              <div class="row">
                  <div class="col-sm-12">
                      <div class="page-header">
                          <h1><?php the_field('contributor_team_title'); ?></h1>
                      </div>
                  </div>
              </div>
              <div class="row profiles">
                  <?php if(have_rows('contributor_team_bio')): $i = 0;
                  while (have_rows('contributor_team_bio')) : $i++; the_row();

                  ?>
                  <?php
                      $modal = get_sub_field('name');
                      $modal = preg_replace("/[^A-Za-z0-9 ]/", '', $modal);
                      $modal = str_replace(" ", "-", $modal);
                      $modal = strtolower($modal);

                      $contribute = get_sub_field('photo');
                      $alt = $contribute['alt'];
                      $size = 'medium';
                      $thumb = $contribute['sizes'][$size];

                      //echo $modal;
                  ?>

                  <div class="col-sm-3">
                      <div class="profile contributor-<?php echo $i; ?>">
                          <img src="<?php echo $thumb; ?>" class="img-circle" alt="<?php echo $alt; ?>" />
                          <h4><?php the_sub_field('name'); ?></h4>
                          <!-- <p><?php //the_sub_field('title'); ?></p> -->
                          <a role="button" href="#" class="btn btn-block btn-primary" data-toggle="modal" data-target="#<?php echo $modal; ?>">View Biography</a>
                      </div>
                  </div>
                  <div class="modal fade" id="<?php echo $modal; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content group">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title" id="myModalLabel"><?php the_sub_field('name'); ?></h4>
                        </div>

                        <div class="modal-body">
                          <div class="thumbnail alignleft">
                              <img class="img-rounded" src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>">
                              <div class="caption">
                                  <!-- <div class="social">
                                      <?php if(get_sub_field('email')): ?><a href="mailto:<?php the_sub_field('email'); ?>" class="email" target="_blank"><span class="fa-stack fa-sm"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-envelope fa-stack-1x fa-inverse"></i></span></a><?php endif; ?>
                                      <?php if(get_sub_field('linkedin')): ?><a href="<?php the_sub_field('linkedin'); ?>" class="linkedin" target="_blank"><span class="fa-stack fa-sm"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-linkedin fa-stack-1x fa-inverse"></i></span></a><?php endif; ?>
                                  </div> -->
                              </div>
                          </div>
                          <?php the_sub_field('bio'); ?>
                        </div>

                      </div>
                    </div>
                  </div>
                  <?php endwhile;endif; ?>
              </div>

          </div>
        </div>
      </section>
    </div>

    <div id="medical-centers">
      <section class="main" role="contentinfo">
        <div class="row">
          <div class="container">
            <div class="row">
              <div class="col-sm-12">
                  <div class="page-header">
                      <h1><?php the_field('medical_centers_title'); ?></h1>
                  </div>
              </div>
            </div>
            <div class="accordion">
              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <?php if(have_rows('medical_centers')): $count = 0;
                while (have_rows('medical_centers')) : the_row(); ?>

                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title"><a data-toggle="collapse" class="collapsed" data-parent="#accordion" data-target="#collapse<?php echo $count; ?>"><?php the_sub_field('title'); ?></a></h3>
                  </div>
                  <div id="collapse<?php echo $count; ?>" class="panel-collapse collapse collapsed">
                    <div class="panel-body">
                      <?php the_sub_field('content'); ?>
                    </div>
                  </div>
                </div>
                <?php $count++; endwhile;endif; ?>

              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 medical-center-more">
                <a href="http://www.ameriburn.org/verification_verifiedcenters.php" class="btn btn-block btn-primary" target="_blank">Find Other Verified Burn Centers &gt;</a>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>

    <div class="meet-the-team model-systems" id="model-systems">
      <section class="main" role="bios">
        <div class="row">
          <div class="container about-us">
              <div class="row">
                  <div class="col-sm-12">
                      <div class="page-header">
                          <h1><?php the_field('model_systems_title'); ?></h1>
                      </div>
                  </div>
              </div>
              <div class="row profiles">
                  <?php if(have_rows('model_systems')): $i = 0;
                  while (have_rows('model_systems')) : $i++; the_row();

                    $image = get_sub_field('image');
                    $alt = $image['alt'];
                    $size = 'full_size';
                    $thumb = $image['sizes'][$size];
                  ?>
                  <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="profile model-system-<?php echo $i; ?>">
                      <img class="img-responsive" src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" />
                      <h4><?php the_sub_field('title'); ?></h4>
                      <p><?php the_sub_field('location'); ?></p>
                      <a href="<?php the_sub_field('link'); ?>" class="btn btn-block btn-primary" target="_blank">Visit Website</a>
                    </div>
                  </div>
                  <?php endwhile;endif; ?>
              </div>

          </div>
        </div>
      </section>
    </div>
    <?php endwhile; ?>

    <a href="#0" class="cd-top">Top</a>
</div>
<?php get_footer(); ?>
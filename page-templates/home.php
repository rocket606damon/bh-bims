<?php
/*
 * Template Name: Home Page
 */
get_header();
if (asalah_post_option('asalah_onepage_scroll') != 'yes') {
    // if(class_exists('RevSliderFront')) {
    //   asalah_rev_slider_wrapper();
    // }

}

?>
<!-- start site content -->
<div class="site_content home">

  <?php
  if (asalah_post_option('asalah_onepage_scroll') != 'yes') {
      //asalah_page_title_holder();
  }
  ?>
  <?php
  remove_filter( 'the_content', 'wpautop' );

  remove_filter( 'the_excerpt', 'wpautop' );
  ?>
  <?php while (have_posts()) : the_post(); ?>
  <?php putRevSlider("home1", "test"); ?>
  <div id="home-featured" class="home-featured">
    <section class="new_section main">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="asalah_row row">
              <?php if(have_rows('home_featured')): $i = 0;
              while(have_rows('home_featured')) : $i++; the_row();
                if($i == 1){
                  $class = 'burnmag-home';
                } elseif($i == 2){
                  $class = 'bsone-home';
                } else {
                  $class = 'libre-home';
                }

                $front = get_sub_field('image');

                $width = get_sub_field('width');

                $alt = get_sub_field('title');
                $alt = preg_replace("/[^A-Za-z0-9 ]/", '', $alt);
                $alt = str_replace(" ", "-", $alt);
                $alt = strtolower($alt);
              ?>
                <div class="col-sm-4 col-md-4 col-lg-4 home-block <?php echo $class; ?>" data-mh="one-third">
                  <div class="service_item clearfix icon_default icon_top text_center service_size_small" data-animation-delay="01">
                    <div class="service_icon image_service_icon"  style=" width: <?php echo $width . 'px'; ?>;">
                      <a>
                        <img class="img-responsive service_image" src="<?php echo $front['url']; ?>"  width='<?php echo $width; ?>' alt="<?php echo $alt; ?>"/>
                      </a>
                    </div>
                    <div class="service_body">
                      <a>
                        <h4 class="service_title title"  style="color:#fff;"><?php the_sub_field('title'); ?></h4>
                      </a>
                      <?php the_sub_field('description'); ?>
                      <a><span class="service_value" ></span></a>
                    </div>
                  </div>
                </div>
              <?php endwhile; endif; ?>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <div id="home-top" class="home-top">
    <section class="new_section main">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="asalah_row row">
              <div class="col-sm-8 col-md-8 col-lg-8 video-home">
                <h4>Featured Videos</h4>
              <!--Video Section-->
              <?php if(have_rows('video_home')): $i = 0;
              while(have_rows('video_home')) : $i++; the_row();

              ?>
                <div class="<?php echo ($i == 1) ? 'video-box-top' : 'video-box-bottom' ?>">
                  <div class="col-sm-5">
                    <div class="embed-responsive embed-responsive-16by9">
                      <iframe class="embed-responsive-item" src="<?php the_sub_field('embed_link'); ?>" width="300" height="150" allowfullscreen="allowfullscreen"></iframe>
                    </div>
                  </div>
                  <div class="col-sm-7">
                    <h3><a href="<?php the_sub_field('youtube_link'); ?>" target="_blank" rel="noopener noreferrer"><?php the_sub_field('video_title'); ?></a></h3>
                    <p class="video-description"><?php the_sub_field('video_description'); ?></p>
                  </div>
                </div>
              <?php endwhile; endif; ?>
              </div>
              <!--End of Video Section-->
              <!--Start of TwitterFeed-->
              <div class="col-sm-4 col-md-4 col-lg-4 twitter-feed">
                <h4>Twitter</h4>
                <?php echo do_shortcode("[custom-twitter-feeds]"); ?>
              </div>
              <!--End of TwitterFeed-->
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <div id="home-middle" class="home-middle">
    <section class="new_section main center-home">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="asalah_row row">
              <!--Start Newsletter Section -->
              <div class="col-sm-6 col-md-6 col-lg-6 our_newsletter">
                <div class="title_shortcode title_wrapper">
                  <h4 class="title">Newsletters</h4>
                  <div class="title_divider title_divider_part"></div>
                </div>

                <?php if(have_rows('newsletter_home')): $i = 0;
                while(have_rows('newsletter_home')) : $i++; the_row();

                  $season = get_sub_field('image');

                ?>
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 <?php echo ($i == 1) ? 'newsletter-new' : '' ?>">
                    <div class="newsletter-container">
                      <a class="newsletter-click" href="<?php the_sub_field('link'); ?>" target="_blank" rel="noopener noreferrer">
                        <img class="alignnone size-full" src="<?php echo $season['url']; ?>" alt="newsletter-img" />
                      </a>
                      <h4><?php the_sub_field('issue'); ?></h4>
                      <p class="news-title"><?php the_sub_field('title'); ?></p>
                      <p class="news-sub"><?php the_sub_field('subtitle'); ?></p>
                    </div>
                  </div>
                <?php endwhile; endif; ?>
                <div class="nudge-space" style="margin-bottom: 25px;"></div>
                <a class="btn btn-block btn-primary newsletter-home" href="#" data-toggle="modal" data-target="#newsletter-signup">Sign Up For Our Newsletter</a>
              </div>
              <!--End Newsletter Section-->
              <!--Start Research Publications-->
              <div class="col-sm-6 col-md-6 col-lg-6 research_pubs">
                <div class="title_shortcode title_wrapper">
                  <h4 class="title">Featured Publications</h4>
                  <div class="title_divider title_divider_part"></div>
                </div>
                <?php if(have_rows('publications_home')): $i = 0;
                while(have_rows('publications_home')) : $i++; the_row();

                ?>
                  <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <div class="numberCircle"><?php echo $i; ?></div>
                  </div>
                  <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                    <h5><?php the_sub_field('title'); ?></h5>
                    <p class="art-authors"><?php the_sub_field('authors'); ?></p>
                    <p class="art-description"><a class="collapsed" href="#featured-pub-<?php echo $i; ?>" data-toggle="collapse">Read Full Description</a></p>
                      <div id="featured-pub-<?php echo $i; ?>" class="collapse">
                        <p><?php the_sub_field('collapse'); ?></p>
                      </div>
                  </div>
                <?php endwhile; endif; ?>
                  </div>
                  <!--End Research Publications-->
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <div id="home-bottom" class="home-bottom">
    <section class="new_section main">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="title_shortcode title_wrapper">
              <h4 class="title">Our Collaborators</h4>
              <div class="title_divider title_divider_part"></div>
            </div>

            <div class="row clearfix partners-home">
            <?php if(have_rows('collaborators_home')): $i = 0;
            while(have_rows('collaborators_home')) : $i++; the_row();
                $image = get_sub_field('image');
                $alt = $image['alt'];
                $size = 'full_size';
                $thumb = $image['sizes'][$size];

                if ($i !== 0 && $i % 4 === 0):
            ?>
            </div>
            <!--Start of row 1-->
            <div class="row clearfix partners-home">
            <?php endif; ?>

              <div class="col-xs-12 col-sm-4 col-md-4 <?php echo 'collaborator' . $i; ?>">
                <div class="member_item">
                  <div class="member_avatar">
                    <a href="<?php the_sub_field('link'); ?>" target="_blank" rel="noopener noreferrer">
                      <img class="img-responsive" src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" />
                    </a>
                  </div>
                  <div class="member_info clearfix">
                    <div class="member_title">
                      <h4 class="member_name"><a href="<?php the_sub_field('link'); ?>" target="_blank" rel="noopener noreferrer"><?php the_sub_field('title'); ?></a></h4>
                      <div class="member_text">
                        <p class="standard"><?php the_sub_field('description'); ?></p>
                      </div>
                      <div class="member_social_info"></div>
                    </div>
                  </div>
                </div>
              </div>

              <?php endwhile; endif; ?>
            </div>

          </div>
        </div>
      </div>
    </section>
  </div>

  <?php endwhile; ?>

</div>
<?php get_footer(); ?>
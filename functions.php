<?php

	function renderSearchForm(){
		global $search_form_counter;
		if ( ! isset( $search_form_counter ) ) $search_form_counter = 0;
		$form_id = $search_form_counter ? '' : ' id="searchform"';
		$text_id = $search_form_counter ? 's_' . $search_form_counter : 's';
		$submit_id = $search_form_counter ? '' : ' id="searchsubmit"';
		$search_form_counter++;
		$html =
			'<form role="search" method="get"' . $form_id . ' class="searchform" action="' . esc_url( home_url( '/' ) )  . '">' ."\n" .
			'	<label class="screen-reader-text assistive-text" id="' . $text_id  . '">' . _e( 'Search', 'simple' )  . '</label>' ."\n" .
			'	<input type="search" class="searchinput field" value="' . get_search_query()  . '" name="s" id="' . $text_id  . '" placeholder="' . esc_attr_e( 'Search', 'simple' )  . '" />' ."\n" .
			'	<input type="submit" class="searchsubmit submit" name="submit"' . $submit_id  . ' value="' . esc_attr_e( 'Search', 'simple' )  . '" />' ."\n" .
			'</form>' ."\n";
		return $html;
	}

	function insert_jump_to_top(){
?>
<script>
// When the user scrolls down 20px from the top of the document, show the button
function scrollFunction() {
	jQuery(document).ready(function(){

		//hide or show the "back to top" link
		jQuery(document).on("scroll", function(){
			var scrollBodyToTop = document.body.scrollTop;
			var scrollElementToTop = document.documentElement.scrollTop;
			if (scrollBodyToTop > 20 || scrollElementToTop > 20) {
				document.getElementById("scrollToTopButton").style.display = "block";
			} else {
				document.getElementById("scrollToTopButton").style.display = "none";
			}
		});

		//smooth scroll to top
		jQuery("#scrollToTopButton").on('click', function(event){
			event.preventDefault();
			jQuery('body,html').animate({scrollTop: 0}, 1000);
		});
	});
}
</script>
<?php
}
add_action('wp_footer', 'insert_jump_to_top', 1);

function insert_jump_to_top_execute(){
?>
<script>
	jQuery(document).ready(function(){

		var a = jQuery('li.page-item a[href="http://www.bh-bims.org/research/"] ul li:eq(0)').remove();
		var title = a.attr("title");
		scrollFunction();
	});
</script>
<?php
}
add_action('wp_footer', 'insert_jump_to_top_execute', 2);

function my_pagename(){
	$url =  trim($_SERVER['REQUEST_URI'], "/");
	if ($url == "research" || $url == "resources"){
		return $url;
	}else{
		$pts = explode("/", $url);
		if ($pts[0] == "research" || $pts[0] == "resources"){
			//echo "<!-- final url:". $pts[0] . " -->\n";
			return $pts[0];
		}
	}
	return $url ;
}

if ( ! function_exists( 'asalah_page_meta_info' ) ) :
function asalah_page_meta_info() {
    global $post;
    // first check if meta info line is enabled in option panel
    if ((asalah_post_option("asalah_page_meta_info") == "show") || (asalah_option("asalah_page_meta_info") && asalah_post_option("asalah_page_meta_info") != "hide")):
        ?>
        <div class="blog_info_box pages_meta_info clearfix">
            <!-- check if post date should be in meta or both meta and label in option panel -->
            <?php if (asalah_cross_option('asalah_page_datetime') != "hide"): ?>
                <div class="blog_box_item">
                    <span class="blog_date meta_item"><i class="fa fa-calendar meta_icon"></i> <?php the_time(get_option('date_format')); ?> - <?php echo get_the_time(); ?></span>
                </div>
            <?php endif; ?>

            <?php if (get_the_category_list()): ?>
                <div class="blog_box_item">
                    <span class="blog_category meta_item"><i class="fa fa-folder-open meta_icon"></i> <?php echo get_the_category_list(', '); ?></span>
                </div>
            <?php endif; ?>

            <?php if ((asalah_post_option("asalah_post_comments") == "show") || (asalah_option("asalah_enable_comments") && asalah_post_option("asalah_post_comments") != "hide")): ?>
                <div class="blog_box_item">
                    <span class="blog_comments meta_item"><i class="fa fa-comments meta_icon"></i> <?php comments_number(__("0 Comments", "asalah")); ?></span>
                </div>
            <?php endif; ?>

            <?php if ((asalah_post_option("asalah_page_author_meta") == "show") || (asalah_option("asalah_page_author_meta") && asalah_post_option("asalah_page_author_meta") != "hide")): ?>
                <div class="blog_box_item">
                    <span class="blog_meta_author meta_item"><span class='author_meta_avatar meta_icon'><?php echo get_avatar(get_the_author_meta('ID'), 12); ?></span> <?php _e('By', 'asalah'); ?> <?php the_author_posts_link(); ?></span>
                </div>
            <?php endif; ?>
        </div>
        <?php
    endif;
    // endif for checking if meta info line is enabled in option panel
}
endif;

if ( ! function_exists( 'asalah_post_meta_info' ) ) :
function asalah_post_meta_info() {
    global $post;
    // first check if meta info line is enabled in option panel
    if ((asalah_post_option("asalah_meta_info") == "show") || (asalah_option("asalah_meta_info") && asalah_post_option("asalah_meta_info") != "hide")):
        ?>
        <div class="blog_info_box post_meta_info clearfix">
            <!-- check if post date should be in meta or both meta and label in option panel -->
            <?php if (asalah_cross_option('asalah_post_datetime') != "hide"): ?>
                <div class="blog_box_item">
                    <span class="blog_date meta_item"><i class="fa fa-calendar meta_icon"></i> <?php the_time(get_option('date_format')); ?> - <?php echo get_the_time(); ?></span>
                </div>
            <?php endif; ?>

            <?php if (get_the_category_list()): ?>
                <div class="blog_box_item">
                    <span class="blog_category meta_item"><i class="fa fa-folder-open meta_icon"></i> <?php echo get_the_category_list(', '); ?></span>
                </div>
            <?php endif; ?>

            <?php if ((asalah_post_option("asalah_post_comments") == "show") || (asalah_option("asalah_enable_comments") && asalah_post_option("asalah_post_comments") != "hide")): ?>
                <div class="blog_box_item">
                    <span class="blog_comments meta_item"><i class="fa fa-comments meta_icon"></i> <?php comments_number(__("0 Comments", "asalah")); ?></span>
                </div>
            <?php endif; ?>

            <?php if ((asalah_post_option("asalah_author_meta") == "show") || (asalah_option("asalah_author_meta") && asalah_post_option("asalah_author_meta") != "hide")): ?>
                <div class="blog_box_item">
                    <span class="blog_meta_author meta_item"><span class='author_meta_avatar meta_icon'><?php echo get_avatar(get_the_author_meta('ID'), 12); ?></span> <?php _e('By', 'asalah'); ?> <?php the_author_posts_link(); ?></a></span>
                </div>
            <?php endif; ?>

            <?php if (asalah_option("asalah_show_estimated_time")): ?>
              <div class="blog_box_item">
                  <span class="blog_time meta_item"> <i class="fa fa-clock-o meta_icon"></i> <?php  echo asalah_estimated_reading_time(); ?></span>
              </div>

            <?php endif; ?>
        </div>
        <?php
    endif;
    // endif for checking if meta info line is enabled in option panel
}
endif;

/**
 * Register Custom Nav Walker
 */
require_once('lib/navwalker.php');

/**
 * Register Header Navigation
 */
register_nav_menu('header', __('Header Menu', 'asalah'));

/**
 * Register Full Size Image
 */
add_theme_support('post-thumbnails');
add_image_size('full_size', 9999, 9999, true);

/**
 * Add <body> classes
 */
function add_body_classes($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  return $classes;
}
add_filter('body_class', 'add_body_classes');

/**
 * Typekit
 */
function typekit_inline() { ?>
<script src="https://use.typekit.net/bod5rqp.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
<?php
  }
add_action( 'wp_head', 'typekit_inline' );

/**
 * Giving editor access to Gravity Forms
 */
function add_grav_forms(){
  $role = get_role('editor');
  $role->add_cap('gform_full_access');
}
add_action('admin_init', 'add_grav_forms');

 /**
 * Get YouTube ID from URL
 */
function get_youtube_id($url) {
    $pattern =
        '%^# Match any YouTube URL
        (?:https?://)?  # Optional scheme. Either http or https
        (?:www\.)?      # Optional www subdomain
        (?:             # Group host alternatives
          youtu\.be/    # Either youtu.be,
        |youtube(?:-nocookie)?\.com  # or youtube.com and youtube-nocookie
          (?:           # Group path alternatives
            /embed/     # Either /embed/
          | /v/         # or /v/
          | /watch\?v=  # or /watch\?v=
          )             # End path alternatives.
        )               # End host alternatives.
        ([\w-]{10,12})  # Allow 10-12 for 11 char YouTube id.
        %x'
        ;
    $result = preg_match($pattern, $url, $matches);
    if (false !== $result) {
        return $matches[1];
    }
    return false;
}

 /**
 * Get Vimeo ID from URL
 */
function get_vimeo_id( $url ) {
    $regex = '~
        # Match Vimeo link and embed code
        (?:<iframe [^>]*src=")?         # If iframe match up to first quote of src
        (?:                             # Group vimeo url
                https?:\/\/             # Either http or https
                (?:[\w]+\.)*            # Optional subdomains
                vimeo\.com              # Match vimeo.com
                (?:[\/\w]*\/videos?)?   # Optional video sub directory this handles groups links also
                \/                      # Slash before Id
                ([0-9]+)                # $1: VIDEO_ID is numeric
                [^\s]*                  # Not a space
        )                               # End group
        "?                              # Match end quote if part of src
        (?:[^>]*></iframe>)?            # Match the end of the iframe
        (?:<p>.*</p>)?                  # Match any title information stuff
        ~ix';

    preg_match( $regex, $url, $matches );

    return $matches[1];
}

/**
 * Gets a vimeo thumbnail url
 * @param mixed $id A vimeo id (ie. 1185346)
 * @return thumbnail's url
*/
function getVimeoThumb($id) {
    $data = file_get_contents("http://vimeo.com/api/v2/video/$id.json");
    $data = json_decode($data);
    return $data[0]->thumbnail_large;
}

 /**
 * Allow for smooth scrolling on About Us Page
 */
function fluid_animation_backtotop() {
?>
	<script>
		jQuery(document).ready(function(){

			function GetCurrentPageSubpageHashVar(varname){
				var retval = {
					page:'',
					subpage:'',
					hash:'',
					query:''
				};

				// start with the whole url
				var page = window.location.href;

				// get the root page
				page = page.replace(window.location.protocol + '//', '');
				page = page.replace(window.location.host, '');
				page = page.replace(window.location.hash, '');
				page = page.replace(window.location.search, '');
				pts = page.split('/');

				retval.page = pts[1]
				retval.subpage = page.replace(retval.page, '');
				retval.subpage = retval.subpage.replace('/', '');
				retval.subpage = retval.subpage.replace('/', '');
				retval.hash = window.location.hash;
				retval.query = GetURLParameter(varname);
				return retval;
			}

			function GetURLParameter(sParam){
				var sPageURL = window.location.search.substring(1);
				var sURLVariables = sPageURL.split("&");
				for (var i = 0; i < sURLVariables.length; i++){
					var sParameterName = sURLVariables[i].split("=");
					if (sParameterName[0] == sParam){
						return sParameterName[1];
					}
				}
				return "";
			}

			// on page load see if we are on the 'about-us' page...
			jQuery(function() {
				var currentPage = window.location.href;
				if (currentPage.indexOf('about-us') !== -1){
					var hash = GetURLParameter('hash');
					if (hash.length > 0){
						jQuery('html, body').animate({
							scrollTop: jQuery('#' + hash).offset().top
						}, 2000);
					}
				}
			});

			jQuery( "ul#menu-header-menu li a" ).on("click", function(event){
				var currentPage = window.location.href;
				if (currentPage.indexOf('about-us') !== -1){
					// catch all "a" clicks on the 'about us' dropdown...

					// this is where we are...
					var sourceInfo = GetCurrentPageSubpageHashVar('hash');

					// init the hash variable
					var hashVariable = '';

					// this is where we want to go!
					var url = jQuery(this).attr('href');
					if (url.indexOf('about-us') !== -1){
						event.preventDefault();
						// see if this is the top non-hashed
						if (url.indexOf("#") >= 0){
							var hashes = url.split('#');
							hashVariable = hashes[1];
							jQuery('html, body').animate({
								scrollTop: jQuery('#' + hashVariable).offset().top
							}, 2000);
						}
					}
				}
			});
		});
	</script>
<?php
}
add_action('wp_footer', 'fluid_animation_backtotop', 20);

/**
 * Special Loading for publication
 */

function publication_animation() {
?>
    <script type="text/javascript">
    var currentPublicationOffset = 0;
    var currentPublicationsCount = 0;

    jQuery.noConflict();
	jQuery(document).ready(function(){

		publicationToggle(1);

		//function loadMore(){
		jQuery(".load-more-publications").on("click", function(e){
			e.preventDefault();
			nextPublicationSegment(10);
			jQuery('html,body').animate({
				scrollTop: jQuery(this).offset().top
			}, 1500);
		});
	//}

      function nextPublicationSegment(segmentLength){
        if (currentPublicationOffset == 0 || currentPublicationOffset + segmentLength <= currentPublicationsCount){
          var start = currentPublicationOffset;
          var finish = currentPublicationOffset + segmentLength;
          if (finish > currentPublicationsCount) finish = currentPublicationsCount;
          for(var i = start; i < finish; i++){
            jQuery('.publication-' + i).slideDown();
            currentPublicationOffset++;
          }
        }
      }

      function publicationToggle(toggleid){
          currentPublicationOffset = 0;
		  currentPublicationsCount = $('.publication').length;
          $('.publication').hide();
          nextPublicationSegment(10);
      }

      function publicationToggle2(toggleid){
        var url = "<?php echo get_stylesheet_directory_uri() ; ?>/lib/publications.html";
    	var data = {toggle:toggleid};
      jQuery.ajax({
        url: url,
        data: data,
        dataType:'json',
        method:'GET',
      }).done(function(data) {
          currentPublicationOffset = 0;
          currentPublications = data.publication;
          jQuery('.publications > .publication').hide();
          nextPublicationSegment(10);
        });
      }

    });
    </script>
  <?php
}
if (is_page('research')){
	add_action('wp_footer', 'publication_animation', 20);
}

/**
 * Special Loading for Videos
 */

function videos_animation() {
    ?><script type="text/javascript">

    jQuery.noConflict();

    var vl = {
		urlSuffix: "/lib/video-select/getVideos.php",
		urlBase: "<?php echo get_stylesheet_directory_uri() ; ?>",
		segmentLength: 2,
		preloadLength: 4,
		loadTag: 'src',
		preLoadTag: 'data-src',
		downloading: false,
		category: 1,
		offset: 0,
		videoCount: 0,
		list: {},
		template:
			'                      <div class="col-sm-12 video video-{{index}}">' + "\n" +
			'                        <div class="col-sm-5">' + "\n" +
			'                          <div class="embed-responsive embed-responsive-16by9">' + "\n" +
			'                            <iframe class="embed-responsive-item" {{pre_tag}}="" {{post_tag}}="{{embedVideoUrl}}" allowfullscreen></iframe>' + "\n" +
			'                          </div>' + "\n" +
			'                        </div>' + "\n" +
			'                        <div class="col-sm-7">' + "\n" +
			'                          <h3><a href="{{bottomVideoUrl}}" target="_blank">{{title}}</a></h3>' + "\n" +
			'                          <p class="video-description">{{description}}</p>' + "\n" +
			'                        </div>' + "\n" +
			'                      </div>' + "\n",
		urlFormat: { vimeo:{
			prefix:'//player.vimeo.com/video/', suffix: '?title=0&amp;byline=0&amp;portrait=0',
				bottomPrefix:'https://vimeo.com/'},
			youtube:{prefix:'//www.youtube.com/embed/', suffix:'',
				bottomPrefix:'//www.youtube.com/watch?v='},
		},
		removals:["http:", "https:", "www.", "vimeo.com", "//", "/",
				"watch?v=", "youtube.com", "youtu.be", "embed"],
		tValues: {
			'Social &amp; Functional Recovery':1,
			'Instructional Therapy':2,
			'Research':3,
			'George\'s Top Ten':4
		}

	};

    jQuery(document).ready(function(){

		// trigger download of videos...
		videoToggle(1);

		jQuery(".load-more-videos").on("click", function(e){
			e.preventDefault();
			loadMoreVideos();
			jQuery('html,body').animate({scrollTop: jQuery(this).offset().top}, 1500);
		});

		jQuery("#video-dropdown-menu li a").click(function(e){
			e.preventDefault();
			var tValues = vl.tValues;
			var selText = jQuery(this).text();
        	jQuery(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>').attr("value", selText);
			var toggleValue = jQuery(this).parents('.btn-group').find('.dropdown-toggle').html();
			toggleValue = toggleValue.replace(' <span class="caret"></span>', '');
			var toggleid = vl.tValues[toggleValue];
			videoToggle(toggleid);
		});

		function preloadVideos(start){
			var finish = (start + vl.preloadLength > vl.videoCount) ?
					vl.videoCount :
					vl.offset + vl.preloadLength;
			for(var index = start; index <= finish; index++){
				var displayIndex = index + 1;
				var video = vl.list[vl.category[index]];

			 	// set the current src if not set
			 	var sel = '.video-'+displayIndex+' iframe';
			 	var src = jQuery(sel).attr('src');
			 	if (typeof(src) === 'undefined'){
			 		var src = "";
				}
				if (src.length == 0){
					jQuery(sel).attr('src', jQuery(sel).data('src'));
				}
			}
		}

		function renderVideos(){
			var html = '';
			jQuery.each(vl.list[vl.category], function(index, video){
				var vars = {};
				var videoUrl = '';

				var yttype = typeof(video.youtube);
				var vtype = typeof(video.vimeo);

				if (video.video_type == 'youtube' &&  yttype == 'string'){
					videoUrl = video.youtube;
				}else if (video.video_type == 'vimeo' && vtype == 'string'){
					videoUrl = video.vimeo;
				}else if (typeof(video.video_url) !== 'undefined'){
					videoUrl = video.video_url;
				}

				if (videoUrl){
					jQuery.each(vl.removals, function(i, str){
						videoUrl = videoUrl.replace(str, '');
					});
					var videoId = videoUrl;
					vars.index = index + 1;
					vars.embedVideoUrl = vl.urlFormat[video.video_type].prefix +
							videoId + vl.urlFormat[video.video_type].suffix;
					vars.bottomVideoUrl = vl.urlFormat[video.video_type].bottomPrefix + videoId;
					vars.description = video.video_description;
					vars.title = video.video_title;

					vars.post_tag = (vars.index <= vl.preloadLength) ? "src" : "data-src";
					vars.pre_tag = (vars.index <= vl.preloadLength) ? "data-src" : "src";

					var videoItem =  vl.template;
					jQuery.each(vars, function(name, value){
						videoItem = videoItem.replace('{{'+name+'}}', value);
					});
					html = html + videoItem;
				}
			});
			jQuery('.video-list').html(html);
		}

		function videoToggle(toggleid){
			jQuery('.videos > .video').hide();
			if (vl.downloading){
				vl.category = toggleid;
				return;
			}

			if (typeof vl.list[toggleid] != "undefined"){
				vl.category = toggleid;
				vl.videoCount = vl.list[vl.category].length;
				vl.offset = 0;
				renderVideos();
				jQuery('.videos > .video').hide();
				loadMoreVideos();
			}else{
				vl.category = toggleid;
				loadVideos();
			}
		}

		function loadVideos(){
			vl.downloading = true;
			var url = vl.urlBase + vl.urlSuffix;
			jQuery.ajax({
				url: url,
				dataType:'json',
				method:'GET'
			}).done(function(videos) {
				vl.offset = 0;
				vl.list = videos;
				vl.downloading = false;
				vl.videoCount = vl.list[vl.category].length;
				renderVideos();
				loadMoreVideos();
			});
		}

		function loadMoreVideos(){
			var start = vl.offset;
			var finish = (vl.segmentLength > vl.videoCount) ?
					vl.videoCount :
					vl.offset + vl.segmentLength;
			preloadVideos(start);
			for(var index = start; index < finish; index++){
				var displayIndex = index + 1;
				jQuery('.video-'+displayIndex).slideDown();
				vl.offset++;
			}
		}
    });
    </script>
  <?php
}
// Do not load unless this is the resources page
if(my_pagename() == 'resources'){
	add_action('wp_footer', 'videos_animation', 20);
}

 /**
 * Allow for smooth scrolling on About Us Page
 */
function font_resizer() { ?>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/font-resize.js"></script>
<?php }
add_action('wp_footer', 'font_resizer', 20);

/**
* Bootstrap multiselect on Publication Page
* ONLY load this for the research page
*/
function load_bootstrap_multiselect() {
?>
	<link href="<?php echo get_stylesheet_directory_uri() ; ?>/lib/xmldocs/bootstrap-multiselect/dist/css/bootstrap-multiselect.css" type="text/css" rel="stylesheet" >
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ; ?>/lib/xmldocs/bootstrap-multiselect/dist/js/bootstrap-multiselect.js"></script>
	<script type="text/javascript">
		jQuery('.years').hide();
		jQuery('#year<?php echo date('Y'); ?>').show();
		jQuery(document).ready(function() {
			jQuery('#bh_bim_publications').multiselect({
				maxHeight: 590,
				buttonWidth: '100%',
				selectedClass: 'multiselect-selected',
				nonSelectedText: 'Selected year(s)',
				onChange: function(element, checked) {
					var lastSelected = element.val();
					var id = lastSelected.replace("demo","year");
					if (checked){
						jQuery(id).show();
					}else{
						jQuery(id).hide();
					}
				}
			});
		});
	</script>
<?php
}

// Do not load unless this is the research page
if(my_pagename() == 'research'){
	add_action('wp_footer', 'load_bootstrap_multiselect', 20);
}

function tabs_function(){
	$upload_dir_2017_04  = wp_upload_dir("2017/04");
	$upload_dir_2017_02  = wp_upload_dir("2017/02");

	if ( ! empty( $upload_dir['url'] ) ) {
    	$user_dirname = $upload_dir['basedir'].'/'.$current_user->user_login;
        if ( ! file_exists( $user_dirname ) ) {
        	wp_mkdir_p( $user_dirname );
    	}
	}
?>
	<script type="text/javascript">
		var uploadDir_2017_04 = "<?php echo $upload_dir_2017_04['url']; ?>";
		var uploadDir_2017_02 = "<?php echo $upload_dir_2017_02['url']; ?>";
		var ptinfo = {
			jumpToSecondaryNav: 0,
			pagename: "",
			bgs: {},
			tabs: {},
			tabsByOffset: [],
			contentChange: false,
			currentPage: false,
			currentHref: '',
			subpageHeaders: {},
			defaultSubpageIndex: 0,
		};

	<?php if(my_pagename() == 'resources'): ?>
		ptinfo.tabs = {
			"peer-support":1,
			"factsheets":2,
			"videos":3,
			"fire-safety-and-prevention":4,
			"disability-and-human-resources":5
		};
		ptinfo.tabsByOffset = [
			"peer-support",
			"factsheets",
			"videos",
			"fire-safety-and-prevention",
			"disability-and-human-resources"
		];
		ptinfo.pagename = "/resources/";
		ptinfo.contentChange = true;

		ptinfo.subpageHeaders = {
			"peer-support":{
				title:"Peer Support",
				image:uploadDir_2017_04 + "/peersupport.jpg",
				text:"Burns are incredibly challenging and often isolating injuries. Many survivors find peer " +
					"support to be one of the most helpful resources in the recovery process. Peer support " +
					"connects survivors and their families with others who have experienced burn injuries and " +
					"who understand the many ups and downs of recovery. Peer supporters can listen, answer questions, " +
					"offer practical advice, and promote a sense of belonging in the community. By sharing their stories, " +
					"peer supporters inspire hope and offer examples of what life is like after a burn injury.",
			},
			"factsheets":{
				title:"Factsheets",
				image:uploadDir_2017_04 + "/factsheets.jpg",
				text:"The factsheets listed below provide useful information on a variety of topics important to burn " +
					"survivors. These materials were created by the Model Systems Knowledge Translation Center and the " +
					"Burn Model Systems and have been reviewed by clinicians and burn survivors to ensure they are up-to-date, " +
					"evidence-based, and consumer-friendly.",
			},
			"videos":{
				title:"Videos",
				image:uploadDir_2017_04 + "/video-section.jpg",
				text:"The videos below provide information on a variety of topics, including therapy exercises, " +
					"social recovery, and research.",
			},
			"fire-safety-and-prevention":{
				title:"Fire Safety and Prevention",
				image:uploadDir_2017_04 + "/fireprevention.jpg",
				text:"Our responsibilities go beyond helping people who already have burns. It is our intent to educate the " +
					"public about fire safe practices and support fire safety legislation.",
			},
			"disability-and-human-resources":{
				title:"Disability and Human Resources",
				image:uploadDir_2017_02+ "/Spaulding-Rehab.jpg",
				text:"This page contains information on resources that may be helpful to you and your family following your " +
					"injury. Many of these services are provided through state and federal government programs while others may " +
					"be available through your work or a nonprofit agency. Independent living centers are run by people with " +
					"disabilities and provide advocacy, information and referral, peer support, skills training, and transitional " +
					"services in order to enhance the independence of people with disabilities.",
			}
		};


	<?php elseif(my_pagename() == 'research'): ?>
		ptinfo.jumpToSecondaryNav = 1;
		ptinfo.bgs = {
			1:'libre-bkg',
			2:'',
			3:'',
			4:'other-bkg'
		};
		ptinfo.tabs = {
			'libre-profile':1,
			'longitudinal-database':2,
			'publications':3,
			'research-other':4
		};
		ptinfo.tabsByOffset = [
			'libre-profile',
			'longitudinal-database',
			'publications',
			'research-other'
		];
		ptinfo.pagename = "/research/";
	<?php endif; ?>


	// check to see whether the selected menu item is on the current page
	function isCurrentPage(newHref){
		var page = newHref.replace(window.location.protocol + "//", '');
		page = page.replace(window.location.hostname , '');
		page = page.replace(window.location.hash, '');
		var ps = page.split('/');
		var page = '/' + ps[1] + '/';
		return (page == ptinfo.pagename) ? true : false;
	}

	// update the content on top of the page if required
	function updateHeaderContent(index){
		if (ptinfo.contentChange){
			var data = ptinfo.tabsByOffset[index-1];
			jQuery("#page-header-title").html(ptinfo.subpageHeaders[data].title);
			jQuery("#page-header-text").html(ptinfo.subpageHeaders[data].text);
			jQuery("#page-header-image").attr('src', ptinfo.subpageHeaders[data].image);
		}
	}

	// preload large images
	function lpPreloadImages(arrayOfImages) {
		var arrayOfImages = [
			// image:uploadDir_2017_04 + "/peersupport.jpg",
			uploadDir_2017_04 + "/factsheets.jpg",
			uploadDir_2017_04 + "/video-section.jpg",
			uploadDir_2017_04 + "/fireprevention.jpg",
			uploadDir_2017_02 + "/Spaulding-Rehab.jpg"
		];
		jQuery(arrayOfImages).each(function(){
			jQuery('<img/>')[0].src = this;
		});
	}

	// reset the current tab
	function resetActiveTopButton(index){
		jQuery(".li-clicker").removeClass('active');
		var activeTab = "#li-tabs-" + index;
		jQuery(activeTab).addClass('active');
		jQuery( "#tabs" ).tabs({ active: index - 1});
	}

	// change/set background images if required
	function backgroundImagesSwitch(index){
		// see if there are background images to switch
		if (Object.keys(ptinfo.bgs).length){

			// get the class to be added
			var bgClass = ptinfo.bgs[index];

			// remove all current background class
			jQuery.each(ptinfo.bgs,function(i, cl){
				if (cl){
					jQuery("#tab-container").removeClass(cl);
				}
			});
			// add new background class
			jQuery("#tab-container").addClass(bgClass);
		}
	}

	// reset the secondary nav (tabs) men u to the top of page if required
	function secondaryNavJump(){
		if (ptinfo.jumpToSecondaryNav){
			if(history.pushState) {
				history.pushState(null, null, '#tabs');
			}else {
				window.location.hash = 'tabs';
			}
		}
	}

	// do all of the top-of-page/tab setup needed on tab/page change
	function setHeaderContent(index){
		// update top title and description
		updateHeaderContent(index);

		// switch bg image(s)
		backgroundImagesSwitch(index);

		// reset active top button
		resetActiveTopButton(index);

		// if set, jump to the secondary nav
		secondaryNavJump();
	}

	jQuery(document).ready(function() {

		// preload large bitmaps
		lpPreloadImages();

		// set up handler for  bottom menu tab clicks....
		jQuery(".li-clicker a").on("click", function(event){
			// find the index (number) of the selected tab
			var id = jQuery(this).attr('id');
			var index = id.replace("tab-clicker", "");

			// set new header/tab
			setHeaderContent(index);
			event.preventDefault();
		});

		// set up handler for top nav menu clicks....
		jQuery(".menu-item a").on("click", function(event){
			// find the index (number) of the selected tab
			// locate the href for this clicked page section
			var href = jQuery(this).attr('href');
			if (href.indexOf(ptinfo.pagename) == -1){
				return;
			}
			var hpts = href.split('/');
			var tabname = hpts[hpts.length-2];
			tabname = (tabname in ptinfo.tabs) ? tabname : Object.keys(tabs)[0];
			var index = ptinfo.tabs[tabname];

			// set new header/tab
			setHeaderContent(index);
			event.preventDefault();
		});

		// handle load from another page...
		// find the index (number) of the selected tab
		var pathname = window.location.pathname;
		var suffix = pathname.replace(ptinfo.pagename, '');
		suffix = (suffix.length == 0) ? Object.keys(ptinfo.tabs)[0] : suffix;
		var tab = suffix.replace('/', '');
		var index = ptinfo.tabs[tab];

		// set new header/tab
		setHeaderContent(index);
		// event.preventDefault();
	});
</script>
<?php
}

// Dont't load unless this is the resources or reseach page
if(my_pagename() == 'resources' || my_pagename() == 'research' ){
	add_action('wp_footer', 'tabs_function', 20);
}

function jquery_ui_tabs_loader(){
	//Enqueue date picker UI from WP core:
	wp_enqueue_script('jquery-ui-tabs');

	//Enqueue the jQuery UI theme css file from google:
	//wp_enqueue_style('e2b-admin-ui-css','http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css',false,"1.9.0",false);
}
add_action('wp_enqueue_scripts', 'jquery_ui_tabs_loader');

function acf_update_publications_value( $value, $post_id, $field  ) {
	require_once("lib/pubmed/renderPublications.php");
    return $value;
}
add_filter('acf/update_value/name=pm_pubs', 'acf_update_publications_value', 10, 3);

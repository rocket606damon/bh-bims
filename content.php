<?php
$current_post_id = "";
if (isset($_POST['current_post_id'])) {
    $current_post_id = $_POST['current_post_id'];
}
$blog_thumbnail_option = "blog";

if (asalah_option("asalah_blog_style") == "masonry") {
    $blog_thumbnail_option = "masonry";
}elseif (asalah_option("asalah_blog_style") == "classic") {
    $blog_thumbnail_option = "bloggrid";
}
if (asalah_post_option("asalah_blog_style", $current_post_id) == "default") {
    $blog_thumbnail_option = "blog";
}elseif (asalah_post_option("asalah_blog_style", $current_post_id) == "masonry") {
    $blog_thumbnail_option = "blogmasonry";
}elseif (asalah_post_option("asalah_blog_style", $current_post_id) == "classic") {
    $blog_thumbnail_option = "bloggrid";
}

$excerpt_length = "40";
if (intval(asalah_option("asalah_blog_excrept_length"))) {
    $excerpt_length = asalah_option("asalah_blog_excrept_length");
}
if (intval(asalah_post_option("asalah_blog_excrept_length", $current_post_id))) {
    $excerpt_length = asalah_post_option("asalah_blog_excrept_length", $current_post_id);
}
?>
<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
<article class="blog_post blog_type<?php echo get_post_format(); ?> filterable_item clearfix post_ID_<?php echo get_the_ID();?>">

    <!-- start blog post banner if exist -->
    <!-- appears only if post format not set to standard -->
    <?php if (get_post_format() != ""): ?>
    <div class="blog_banner clearfix">
        <header class="content_banner blog_post_banner clearfix">
            <?php
            if (is_single() || is_page()) :
                if (asalah_option("asalah_crop_single_banner")) {
                    asalah_blog_post_banner("blog");
                }else{
                    asalah_blog_post_banner();
                }

            else:
                if (asalah_option("asalah_crop_blog_banner")) {
                    asalah_blog_post_banner($blog_thumbnail_option);
                }else{
                    asalah_blog_post_banner();
                }
            endif;
            ?>
        </header>
    </div>
  <?php elseif (has_post_thumbnail() && !is_single()): ?>
    <div class="blog_banner clearfix">
        <header class="content_banner blog_post_banner clearfix">
            <?php

                if (asalah_option("asalah_crop_blog_banner")) {
                    asalah_blog_post_banner($blog_thumbnail_option);
                }else{
                    asalah_blog_post_banner();
                }

            ?>
        </header>
    </div>
  <?php endif;?>
    <!-- end blog post banner if exist -->
    <!-- Reader view -->

    <?php if (asalah_option('asalah_reader_view') && is_single()):?>
      <div class="reader_view_container <?php if (asalah_option('asalah_post_icons')) { echo 'post_icon';} ?>">


        <div class="reader_options">
          <a href="javascript: void(0)" class="btn btn-default web-view">Revert to Web View</a>
          <label>Color Scheme</label>
          <select name="color-scheme">
            <option value="default">Default</option>
            <option value="inverse">Inverse</option>
          </select>
          <label>Font Size</label>
          <select name="font-size">
            <option value="medium">Medium</option>
            <option value="large">Large</option>
            <option value="small">Small</option>
          </select>
          <label>Font Type</label>
          <select name="font-type">
            <option value="serif">Serif</option>
            <option value="sans-serif">Sans Serif</option>
          </select>

        </div>
      </div>
    <?php endif;?>
    <?php if (asalah_option('asalah_reader_view') && is_single()):?>
      <a href="javascript: void(0)" class="btn btn-default <?php if (asalah_option('asalah_post_icons')) { echo post_icon;} ?>" id="reader-view">Reader View</a>
    <?php endif;?>

    <div class="blog_info clearfix">

        <!-- get post icon -->
        <?php
        // if the current page is page don't show post type
        if (!is_page()) {
            asalah_post_icon_label();
        }
        ?>

        <!-- end post icon -->
        <div class="blog_body_wrapper">

            <div class="blog_heading">

                <?php if (is_single() || is_page()) : ?>
                  <?php if (is_single( )) { $title_tag = 'h1'; } else { $title_tag = 'h3';} ?>
                    <?php if (asalah_post_option("asalah_post_title") != 'hide'): ?>
                    <div class="blog_title">
                        <<?php echo $title_tag ?> class="title blog_post_title"><?php the_title(); ?></<?php echo $title_tag; ?>>
                    </div>
                    <?php endif; ?>
                <?php else : ?>
                    <?php if (get_the_title()): ?>
                    <div class="blog_title">
                        <h3 class="title blog_post_title">
                            <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
                        </h3>
                    </div>
                    <?php else: ?>
                    <div class="blog_title">
                        <h3 class="title blog_post_title">
                            <a href="<?php the_permalink(); ?>" rel="bookmark"><?php _e("Go to post", "asalah"); ?></a>
                        </h3>
                    </div>
                    <?php endif; ?>
                <?php endif; // is_single() ?>

                <?php //asalah_post_meta_info(); ?>

                <?php
                if ((asalah_post_option("asalah_post_share") == "show") || (asalah_option("asalah_post_social_share") && asalah_post_option("asalah_post_share") != "hide")) {
                  if ((asalah_cross_option('asalah_social_share_postion') == 'above') && (is_single() || is_page()) ) {
                    asalah_post_share('above');
                  }
                } ?>

                <div class="blog_content_wrapper shifted_description">
                    <?php
                    $is_sticky_share = "no_sticky_share";
                    if (is_single() || is_page()) {
                      if ((asalah_cross_option('asalah_social_share_postion') != 'above') && (asalah_cross_option('asalah_social_share_postion') != 'below')) {
                        asalah_post_share();
                          if ((asalah_post_option("asalah_post_share") == "show") || (asalah_option("asalah_post_social_share") && asalah_post_option("asalah_post_share") != "hide")) {
                            $is_sticky_share = "has_sticky_share";
                          }
                      }

                    }
                    ?>
                    <div class="<?php echo $is_sticky_share ?> blog_description">
                        <!-- start show post description -->
                        <!-- if current page is single or page show content, else show only excerpt. -->
                        <?php if (is_single() || is_page()) : ?>

                            <div class="post_content">
                            <?php the_content(__('Continue reading <span class="meta-nav">&rarr;</span>', 'asalah')); ?>
                            <?php wp_link_pages(array('before' => '<div class="page-links"><span class="page-links-title">' . __('Pages:', 'asalah') . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>')); ?>
                            </div>
                        <?php
                        else:
                            if (asalah_post_option('asalah_custom_post_description') != '') :
                              echo asalah_post_option('asalah_custom_post_description');
                            else:
                              echo '<p>' . the_excerpt() . '</p>';
                            endif;
                        endif;

                        if (is_single() || is_page()) {
                            wp_reset_query();
                        }
                        ?>
                        <!-- end show post description -->

                        <!-- start tag cloud -->
                        <?php if (((asalah_post_option("asalah_post_tags") == "show") || (asalah_option("asalah_post_tags") && asalah_post_option("asalah_post_tags") != "hide")) && get_the_tags()): ?>
                            <div class="tagcloud blog_post_tags clearfix"><?php the_tags("", "", ""); ?></div>
                        <?php endif; ?>
                        <!-- end tag cloud -->
                    </div> <!-- end blog_description -->

                    <?php
                    if ((asalah_cross_option('asalah_post_ads_enabled') != 'hide') && (is_single())) {
                    /* Google Ads Theme Options */
                    $devices = array('all', 'desktop', 'tablet', 'mobile');
                    foreach ($devices as $device) {
                      if (asalah_cross_option('asalah_post_ads_'.$device)) {
                        echo asalah_insert_ad(asalah_cross_option('asalah_post_ads_'.$device), 'auto', '0', '', '', $device);
                      }
                    }
                  }
                    ?>

                </div> <!-- end blog_content_wrapper -->
                <?php
                if ((asalah_post_option("asalah_post_share") == "show") || (asalah_option("asalah_post_social_share") && asalah_post_option("asalah_post_share") != "hide")) {
                  if ((asalah_cross_option('asalah_social_share_postion') == 'below') && (is_single() || is_page()) ) {
                    asalah_post_share('below');
                  }
                } ?>
                <?php if (asalah_option('asalah_reader_view') && is_single()):?>
                  <a href="javascript: void(0)" class="btn btn-default web-view" style="margin-top:40px;" >Revert to Web View</a>
                <?php endif; ?>
            </div>

        </div>

    </div>

    <!-- start Custom post meta data -->

    <?php if ((asalah_post_option('asalah_post_source_url') != '') && (is_single())): ?>
      <!-- Source links -->
      <div class="post_source_links_container">
        <bold>Source:</bold> <a href="<?php echo asalah_post_option('asalah_post_source_url');?>"><?php echo asalah_post_option('asalah_post_source_title'); ?></a>
      </div>
    <?php endif; ?>

    <!-- start post navigation -->
    <?php if (is_single()): ?>
    <div class="post_navigation_container <?php if (asalah_post_option('asalah_post_source_title') != '') { echo "post_nav_source_container";}?>">
    <?php if (asalah_cross_option('asalah_post_next_prev') != "hide") : ?>
        <?php
            $next_post = get_next_post();
            $prev_post = get_previous_post();
        ?>
        <?php if ( !empty( $prev_post ) || !empty( $next_post ) ): ?>

                <div class="post_navigation_wrapper clearfix">
                    <?php if (!empty( $prev_post )): ?>
                        <a title="<?php echo esc_attr($prev_post->post_title); ?>" href="<?php echo get_permalink( $prev_post->ID ); ?>" id="right_nav_arrow" class="cars_nav_control next_nav_arrow"><?php _e("Next Post", 'asalah'); ?></i></a>
                    <?php endif; ?>

                    <?php if (!empty( $next_post )): ?>
                        <a title="<?php echo esc_attr($next_post->post_title); ?>" href="<?php echo get_permalink( $next_post->ID ); ?>" id="left_nav_arrow" class="cars_nav_control prev_nav_arrow"><?php _e("Previous Post", 'asalah'); ?></a>
                    <?php endif; ?>
                </div>

        <?php endif; ?>
    <?php endif; ?>
    </div>
    <?php endif; ?>
    <!-- end post navigation -->

</article>

<?php
if (is_single() || is_page()):
    if ((asalah_post_option("asalah_author_box") == "show") || (asalah_option("asalah_author_box") && asalah_post_option("asalah_author_box") != "hide")) {
        asalah_author_box();
    }

    if ((asalah_post_option("asalah_post_comments") == "show") || (asalah_option("asalah_enable_comments") && asalah_post_option("asalah_post_comments") != "hide")) {
        comments_template();
    }
endif;
?>

<?php if ((asalah_cross_option('asalah_meta_info_icons') == 'show') || ((asalah_post_option('asalah_meta_info_icons') == 'default') && (asalah_option('asalah_meta_info_icons') != 'hide')) ) { ?>
  <script type="text/javascript"> jQuery('.post_ID_<?php echo get_the_ID();?> .post_meta_info .meta_item').each( function() {
    jQuery(this).addClass('has_icon');
  }); </script>
<?php } ?>


<?php endwhile;


/* Reader View */

if ( asalah_option('asalah_reader_view')) {


    if (is_single()) {
      ?>
      <script type="text/javascript">
        jQuery(document).ready( function($) {
          jQuery('#reader-view').click( function() {
          jQuery( document.createElement('link') ).attr({
              href: "<?php echo get_template_directory_uri(); ?>/reader.css",
              media: 'screen',
              type: 'text/css',
              rel: 'stylesheet',
              title: 'readerview'
          }).appendTo('head');
          jQuery('.post_content').addClass('medium');
          jQuery('.post_content').addClass('sans_font');
          jQuery('.main_content').removeClass('col-md-9');
          jQuery('.main_content').addClass('col-md-12');
          jQuery('.side_content').hide();
          jQuery(this).hide('fast');
          jQuery('.reader_view_container').show('fast');
          jQuery('.web-view').show('fast');
          $('link[title="readerview"]').prop('disabled', false);
        });

        jQuery('.web-view').click( function() {
          $('link[title="readerview"]').prop('disabled', true);
          $('#reader-view').show('fast');
          $('.web-view').hide('fast');
          $('.reader_view_container').hide('fast');
          <?php if (asalah_post_option('asalah_post_layout') == "left") { ?>
              $('main_content').removeClass('col-md-12');
              jQuery('.main_content').addClass('col-md-9');
              jQuery('.side_content').show();
          <?php } elseif (asalah_post_option('asalah_post_layout') == "full") { ?>
            jQuery('.main_content').removeClass('col-md-9');
            jQuery('.main_content').addClass('col-md-12');
          <?php } elseif (asalah_post_option('asalah_post_layout') == "right") { ?>
            $('main_content').removeClass('col-md-12');
            jQuery('.main_content').addClass('col-md-9');
            jQuery('.side_content').show();
          <?php }?>
          <?php if (asalah_option("asalah_sidebar_position") == "left") { ?>
              $('main_content').removeClass('col-md-12');
              jQuery('.main_content').addClass('col-md-9');
              jQuery('.side_content').show();
          <?php } elseif (asalah_option("asalah_sidebar_position") == "no-sidebar") { ?>
            jQuery('.main_content').removeClass('col-md-9');
            jQuery('.main_content').addClass('col-md-12');
          <?php } elseif (asalah_option("asalah_sidebar_position") == "right") { ?>
            $('main_content').removeClass('col-md-12');
            jQuery('.main_content').addClass('col-md-9');
            jQuery('.side_content').show();
          <?php }?>
        });

        $('select[name="color-scheme"]').change( function($) {
        if ( jQuery('select[name="color-scheme"]').val() == "default") {
          jQuery('body').removeClass('inverse_scheme');
          jQuery('body').addClass('normal_scheme');
        } else if ( jQuery('select[name="color-scheme"]').val() == "inverse") {
          jQuery('body').removeClass('normal_scheme');
          jQuery('body').addClass('inverse_scheme');
        }
      });

        $('select[name="font-size"]').change( function() {
          if ( jQuery(this).val() == "large") {
            jQuery('.post_content').removeClass('small');
            jQuery('.post_content').removeClass('medium');
            jQuery('.post_content').addClass('large');
          } else if ( jQuery(this).val() == "medium") {
            jQuery('.post_content').removeClass('small');
            jQuery('.post_content').removeClass('large');
            jQuery('.post_content').addClass('medium');
          } else {
            jQuery('.post_content').removeClass('large');
            jQuery('.post_content').removeClass('medium');
            jQuery('.post_content').addClass('small');
          }
        });

        $('select[name="font-type"]').change( function() {
          if ( jQuery(this).val() == "serif") {
            jQuery('.post_content').removeClass('sans_font');
            jQuery('.post_content').addClass('serif_font');
          } else {
            jQuery('.post_content').removeClass('serif_font');
            jQuery('.post_content').addClass('sans_font');
          }
        });

      });
    </script>
    <?php
  }

}

/* Reading progress bar */
if (asalah_option('asalah_reading_progress')) {
  if ( is_single()) {
      ?>
      <script type="text/javascript">
        jQuery(document).ready( function($) {



          if ('max' in document.createElement('progress')) {

            var getMax = function(){
           return $('#all_site').height() - $(window).height() - $('.site_header').height();
       }

       var getValue = function(){
           return $(window).scrollTop() - $('.site_header').height();
       }
      var progressBar = $('#reading_progress');

      progressBar.attr({ max: getMax() });

      $(document).on('scroll', function(){
          progressBar.attr({ value: getValue(), max: getMax() });
      });

      $(window).resize(function(){
          // On resize, both Max/Value attr needs to be calculated
          progressBar.attr({ max: getMax(), value: getValue() });
      });

      $('a.btn').click(function(){
        // On resize, both Max/Value attr needs to be calculated
        progressBar.attr({ max: getMax(), value: getValue() });
      });

  } else {

      var progressBar = $('.reading-progress-bar');
      var max = getMax();
      var value;
      var width;

      var getWidth = function() {
          value = getValue();
          width = (value/max) * 100;
          width = width + '%';
          return width;
      }

      var setWidth = function(){
          progressBar.css({ width: getWidth() });
      }

      $(document).on('scroll', setWidth);
      $(window).on('resize', function(){
          max = getMax();
          setWidth();
      });
  }


            });
      </script>

      <?php

  }
} ?>
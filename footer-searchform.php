<form id="searchform" method="get" action="<?php echo esc_url(home_url('/')); ?>">
	<div class="input-group col-md-12">
		<input type="text" class="form-control input-lg" placeholder="<?php _e( 'Hit enter to search', 'asalah' ); ?>" name="s">
	</div>

</form>
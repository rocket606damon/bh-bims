<?php
function getStrsBetween($s,$s1,$s2=false,$offset=0) {
/*====================================================================
Function to scan a string for items encapsulated within a pair of tags
getStrsBetween(string, tag1, <tag2>, <offset>
If no second tag is specified, then match between identical tags
Returns an array indexed with the encapsulated text, which is in turn
a sub-array, containing the position of each item.
Notes:
strpos($needle,$haystack,$offset)
substr($string,$start,$length)
====================================================================*/
if( $s2 === false )
{ $s2 = $s1; }
$result = array();
$L1 = strlen($s1);
$L2 = strlen($s2);
if( $L1==0 || $L2==0 )
{ return false; }
do {
$pos1 = strpos($s,$s1,$offset);
if( $pos1 !== false ) {
$pos1 += $L1;
$pos2 = strpos($s,$s2,$pos1);
if( $pos2 !== false ) {
$key_len = $pos2 - $pos1;
$this_key = substr($s,$pos1,$key_len);
if( !array_key_exists($this_key,$result) )
{ $result[$this_key] = array(); }
$result[$this_key][] = $pos1;
$offset = $pos2 + $L2;
} else
{ $pos1 = false; }
}
} while($pos1 !== false );
return $result;
}
// list of all tags and types in the documents
$tags = array(
"accepted" => array('type' => "Date"),
"ArticleIds" => array('type' => "List", 'attrs' =>
	array(
		array('name' => 'pubmed', 'type' => 'String'),
		array('name' => 'doi', 'type' => 'String'),
		array('name' => 'rid', 'type' => 'String'),
		array('name' => 'eid', 'type' => 'String')
	)
),
"AuthorList" => array('type' => "List", array(
		array('name' => 'Author', 'type' => 'String')
	)
),
//"doi" => array('type' => "String"),
//"eid" => array('type' => "String"),
"ELocationID" => array('type' => "String"),
//"entrez" => array('type' => "Date"),
"EPubDate" => array('type' => "Date"),
"ESSN" => array('type' => "String"),
"FullJournalName" => array('type' => "String"),
"HasAbstract" => array('type' => "Integer"),
"History" => array('type' => "List", 'attrs' =>
	array(
		array('name' => 'pubmed', 'type' => 'String'),
		array('name' => 'medline', 'type' => 'String'),
		array('name' => 'entrez', 'type' => 'String')
	)
),
"ISSN" => array('type' => "String"),
"Issue" => array('type' => "String"),
"Lang" => array('type' => "String"),
"LangList" => array('type' => "List", 'attrs' => array(
		array('name' => 'Lang', 'type' => 'String')
	)
),
"LastAuthor" => array('type' => "String"),
"medline" => array('type' => "Date"),
"NlmUniqueID" => array('type' => "String"),
"Pages" => array('type' => "String"),
"pii" => array('type' => "String"),
"PmcRefCount" => array('type' => "Integer"),
"PubDate" => array('type' => "Date"),
"pubmed" => array('type' => "String"),
"PubStatus" => array('type' => "String"),
"PubType" => array('type' => "String"),
"PubTypeList" => array('type' => "List", 'attrs' => array(
		array('name' => 'PubType', 'type' => 'String'),
	)
),
"received" => array('type' => "Date"),
"RecordStatus" => array('type' => "String"),
"References" => array('type' => "List"),
"revised" => array('type' => "Date"),
//"rid" => array('type' => "String"),
"SO" => array('type' => "String"),
"Source" => array('type' => "String"),
"Title" => array('type' => "String"),
"Volume" => array('type' => "String")
);
// list opf ID's you listed in message
$ids = array(
'28712798',
'28678034',
'28641914',
'28493205',
'28483656',
'28481759',
'28481758',
'28338518',
'28336430',
'28328665',
'28134680',
'27828837',
'27424092',
'27404335',
'27404164',
'27388881',
'27380122',
'27380121',
'27359189',
'27355656',
'27348865',
'27294859',
'27294853',
'27215148',
'27003738',
'26994787',
'26953515',
'26803370',
'26599009',
'26496115',
'26477477',
'26409198',
'26284638',
'26244792',
'25986908',
'25956826',
'25725572',
'25691236',
'25536085',
'25501787',
'25410086',
'25251252',
'25171665',
'25162949',
'25094007',
'25085093',
'24917849',
'24880057',
'24676366',
'24582616',
'23835626',
'23799485',
'23511284',
'23511282',
'23473701',
'23292595',
'23270165',
'23188249',
'23171100',
'23142348',
'23089617',
'23077593',
'22981003',
'22378335',
'22217746',
'22119446',
'21941195',
'21624720',
'21602700',
'24043332',
'21228711',
'19619032',
'19433419',
'19165112',
'18535473',
'18306337',
'17541504',
'17202580',
'16819359',
'16819356',
'16452485',
'16282164',
'15801769',
'15221728',
'15213481',
'14650516',
'14527932',
'14514529',
'12929791',
'12819535',
'12659964',
'12623501',
'12560211',
'12362119',
'12124234',
'11936533',
'11529286',
'11346450',
'11310312',
'10904032',
'10635009',
'7766672',
'8222018',
'16668954',
'1404493',
'1656218',
'2005797',
'1953345',
'2365889',
'3033605',
'3805469',
'3802364',
'24247455',
'3002649',
'6591142',
'6152773',
'7160033',
'28568429',
'346598',
'4709118',
'4885544'
);
// URL for reading pubmed xml items
$url = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=pubmed&id=';
// local fgolder to store the documents
// Note: NCBI docyuments do not change so no need tpo get them every time you want to display.
$dirname = "xmldocs";
// Uncomment below to download the documents from NCBI.
//if (!is_dir($dirname)){
//	mkdir ($dirname); //
//}
//foreach ($ids as $id){
//	$pmurl = $url . trim($id, "\n\t\r ");
//	echo "$id:\n\t$pmurl\n";
//	file_put_contents("$dirname/PM.$id.xml",
//			file_get_contents($pmurl));
//}

function getItem($xml, $tagname, $data, $list=false){
	$prefix = $list ? "\n\t" : "\n\t\t";
	$value = '';
	// ignore the list type elements; will add if needed.
	// get the tag type
	$type = $data['type'];
	// format start tag
	$startTag = $prefix . "<Item Name=\"$tagname\" Type=\"$type\">";
	// format end tag
	$endTag = $prefix."</Item>";
	// turn on for debug
	//echo "$startTag, $endTag\n";
	// get the tag
	$retvals = getStrsBetween($xml, $startTag, $endTag);
	if (is_array($retvals)){
	// load the values if returned
		$items = array_keys($retvals);
		// extract just the first found.
		if (count($items) == 1){
			$value = $items[0];
		}else{
			$value = $items;
	}
	return $value;
}
// read the DocSum item conteaing the data
function getList($xml, $tagname, $data){
	$summary = '';
	// ignore the list type elements; will add if needed.
	$type = 'List';
	// format start tag
	$startTag = "\n\t<Item Name=\"$tagname\" Type=\"$type\">";
	// format end tag
	$endTag = "\n\t</Item>";

	$values = array();
	$retvals = getStrsBetween($xml, $startTag, $endTag);
	if (is_array($retvals)){
		$items = array_keys($retvals);
		$listContent = $items[0];
		foreach($data['attrs'] as $subtag){
			$subtagname = $subtag['name'];
			$values[$subtagname] = getItem($listContent, $subtagname, $subtag, true);
		}
	}
	return $values;
}

// read the DocSum item conteaing the data
function getsummary($xml){
	$summary = '';
	// ignore the list type elements; will add if needed.
	$startTag = "<DocSum>";
	$endTag = "</DocSum>";
	$retvals = getStrsBetween($xml, $startTag, $endTag);
	if (is_array($retvals)){
		$items = array_keys($retvals);
		$summary = $items[0];
	}
	return $summary;
}

// process each xml document
foreach ($ids as $id){
	// read the document from the local directory
	$xml = file_get_contents("$dirname/PM.$id.xml");
	// echo "$xml\n";
	// extract the summary element where coontent is set
	$summary = getSummary($xml);
	//echo $summary;exit;
	// go through the tags and get each one.
	// again, list not implemented but can be.
	//print_r($summary);
	foreach($tags as $tag => $data){
		$output[$tag] = $data['type'] == 'List' ?
			getList($summary, $tag, $data) :
			getItem($summary, $tag, $data);
		//echo $tag."=" . $output[$tag] . "\n";
	}
	// remove this to process all of the itsms
	exit;
}
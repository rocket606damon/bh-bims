<?php

require_once("wp-blog-header.php");
register_activation_hook(__FILE__, 'pubmed_update_activation');

function pubmed_update_activation() {
    if (! wp_next_scheduled ( 'pubmed_update_event' )) {
		wp_schedule_event(time(), 'daily', 'pubmed_update_event');
    }
}

add_action('pubmed_update_event', 'do_pubmed_update');

function getPubmedAuthors(){
	// $authors = get_field('pubmed_auhtor_list');
	$authors = array(
		"Schneider JS",
		"Goverman J",
		"Stoddard FJ Jr",
		"Ryan CM",
		"Goverman J",
		"Kazis LE",
		"Shih SL",
		"Silver JK"
	);
	return $authors;
}

function getPubmedFeedUrl(){
	// $authors = get_field('pubmed_feed_url');
	$pubmedFeed = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/erss.cgi?rss_guid=1jeAVjyuKpXfEVWVZPR8OGpvf0EaymsL-Hg1MbxA2vOUpAbk5C";
	return $pubmedFeed;
}

function getPubmedEmail(){
	// $authors = get_field('pubmed_email_alert');
	$email = 'bill@launchpad.co';
	return $email;
}

function readPubmedFeed($pubmedFeed, $authorList){
	//get page contents from PubMed feed
	$feedstr = file_get_contents($pubmedFeed);
	if($feedstr==false) {
		echo 'Could not retrieve feed contents.'; return ob_get_clean();
	};

	//convert page string to xml object
	$xml = new SimpleXMLElement($feedstr, LIBXML_NOCDATA);

	//query for all channel/item tags
	$res = $xml->xpath('channel/item');

	$publications =  array();
	while(list( , $node) = each($res)) {

		// find url for link
		$hrefs = $node->xpath('link');
		$href = (string) $hrefs[0];

		// find publication title
		$titles = $node->xpath('title');
		$title = $titles[0];

		// find publication title
		$guids = $node->xpath('guid');
		$pmid = str_replace("PMID: ", '', $pmid);
		$pmid = str_replace("PubMed:", '', $guids[0]);

		// find author list
		$auths = $node->xpath('author');
		$authors = trim(implode(",", $auths), " ,");
		$authorNames = explode(",", $authors);

		//Journal name is in a CDATA formatted block of html
		$cdatablock = $node->xpath('description');
		$doc = new DOMDocument();
		$doc->loadHTML($cdatablock[0]);
		$xp = new DOMXpath($doc);
		$el = $xp->query('*/p');
		$journal = $el->item(1)->nodeValue;

		// Burn Institute author cited
		$featured = '';
		foreach($authorNames as $a){
			$a = trim($a);
			if (isset($authorList[$a])){
				echo "found featured: $a\n";
				$featured = $a;
			}
		}

		// Pubmed Citation
		$citation  =  "$authors .$title. $journal. Pubmed PMID $pmid.";

		$info = array(
			'citation' => $citation,
			'href' => $href,
			'featured' => $featured,
			'pmid' => $pmid
		);
		$publications[$pmid] = $info;
	}
	return $publications;
}

function do_pubmed_alert($email) {
	$subject = 'Daily pubmed update';
	$body = 'Daily pubmed update was run';
	wp_mail( $pubmedEmail, $subject, $body);
}

function hasPubmedPMID($pmid){
	// $publications = get_field('pubmed_publications');
	// foreach($publications as $p){
	//	$feedPmid = get_field('pmid');
	//	if ($feedPmid == $pmid){
	//		return true;
	//	}
	//}
	return false;
}

function addPubmedArticle($pmid, $citation, $href){
	$values = array(
		'pmid' => $pmid,
		'citation' => $citation,
		'href' => $href,
		'status' => 'new',
	);
	// add_subfield('pubmed_publications', $values);
}

function do_pubmed_update() {
	// do something every hour
	$authors = getPubmedAuthors();
	$pubmedFeed  = getPubmedFeedUrl();
	$pubmedEmail  = getPubmedEmail();
	$publications = readPubmedFeed($pubmedFeed, $authors);
	$matches = false;
	foreach ($publications as $p){
		$pmid = isset($p['pmid']) ? $p['pmid'] : '';
		$href = isset($p['href']) ? $p['href'] : '';
		$citation = isset($p['citation']) ? $p['citation'] : '';
		if (!hasPubmedPMID($pmid)){
			addPubmedArticle($pmid, $citation, $href);
			do_pubmed_alert($pubmedEmail);
		}
	}
}


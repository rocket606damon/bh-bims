
var currentOffset = 0;
var currentVideos = [];
jQuery.noConflict();
jQuery(document).ready(){
		videoToggle();

		jQuery(".load-more-videos").on("click", function(e){
			e.preventDefault();
			nextSegment(1);
			jQuery('html,body').animate({
				scrollTop: jQuery(this).offset().top
			}, 1500);
		});

		function nextSegment(segmentLength){
			if (currentOffset == 0 || currentOffset + segmentLength <= currentVideos.length){
				var start = currentOffset;
				var finish = currentOffset + segmentLength;
				if (finish > currentVideos.length) finish = currentVideos.length;
				for(var i = start; i < finish; i++){
					var video = currentVideos[i];
					jQuery('.video-'+video.video_index).slideDown();
					currentOffset++;
				}
			}
		}

		function videoToggle(){
			var base_url = "/wp-content/themes/snerscic";
			var url = base_url + "/lib/video-select/getvideos.php";
			jQuery.ajax({
				url: url,
				data: '',
				dataType:'json',
				method:'GET',
			}).done(function(data) {
			currentOffset = 0;
			currentVideos = data.videos;
			jQuery('.videos > .video').hide();
				nextSegment(2);
			});
		}
));

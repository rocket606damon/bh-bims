<?php

define ('WP_LOADFILE_DIR', '../../../../../');
define( 'WP_USE_THEMES', false );
require_once( WP_LOADFILE_DIR . 'wp-load.php' );

$key = 'toggle';
$_GET[$key] = 2;
if (isset($_GET[$key])){
	$value = $_GET[$key];
	$gv = new GetVideos($value);
	$videos = $gv->getVideoList();
	if ($videos != -1){
		echo $videos;
	}
}

class GetVideos {

	var $videos = array();
	var $finalVideos = array();
	var $filterColumns = array();
	var $wpdb = '';
	var $toggle = '';

	function __construct($toggle){
		// echo "__construct\n";
		$this->init($toggle);
	}

	function init($toggle){
		global $wpdb;

		$this->wpdb = $wpdb;
		$this->toggle = $toggle;
		$this->filterColumns = array(
			'video_index',
			//'video_url',
			//'video_title',
			//'video_publish_date',
			//'video_description',
			//'video_type',
			//'youtube',
			//'vimeo',
			'video_select'
		);
	}

	function getVideoList(){
		// echo "getVideoList\n";
		$this->get();
		$this->filter();
		return $this->encode();
	}

	function encode(){
		// echo "encode\n";
		return json_encode(
			array(
				'toggle' => $this->toggle,
				'videos' => $this->finalVideos
			)
		);
	}

	function get(){
		// echo "get\n";
		$rows = $this->wpdb->get_results(
				"SELECT post_id FROM " . $this->wpdb->postmeta . " " .
				"WHERE meta_key LIKE 'videos_%select' " .
				"AND meta_value='" . $this->toggle . "'",
			OBJECT
		);
		foreach($rows as $row){
			$this->getVideos($row);
		}
	}

	function getVideos($row){
		// echo "getVideos\n";
		$videos = $this->wpdb->get_results(
				"SELECT * FROM ".$this->wpdb->postmeta." " .
				"WHERE post_id='" . $row->post_id . "' " .
				"AND meta_key LIKE 'videos_%'",
			OBJECT
		);
		// echo "getVideos: " . count($videos) . "\n";
		foreach($videos as $video){
			$this->index($video);
		}
	}

	function index($video){
		$kp = explode('_', $video->meta_key);
		$index = $kp[1];
		$prefix = "videos_" . $index . "_";
		$short_key = str_replace($prefix, '', $video->meta_key);
		if ($short_key == 'video_select' || in_array($short_key, $this->filterColumns)){
			$this->videos[$index][$short_key] = $video->meta_value;
			$this->videos[$index]['video_index'] = $index +1;
		}
	}

	function filter(){
		// echo "filter\n";
		$this->finalVideos = array();
		foreach($this->videos as $id => $video){
			if ($video['video_select'] == $this->toggle){
				unset($video['video_select']);
				$this->finalVideos[] = $video;
			}
		}
	}
}


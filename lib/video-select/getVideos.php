<?php

define ('WP_LOADFILE_DIR', '../../../../../');
define( 'WP_USE_THEMES', false );
require_once( WP_LOADFILE_DIR . 'wp-load.php' );

function getVideos(){
	$ms = mysqli_connect (DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
	$rs = mysqli_query($ms, "select meta_key, meta_value from (select * from bh_postmeta where post_id=298 AND meta_key like 'videos_%' ORDER BY meta_id ASC) AS x GROUP BY meta_key");
	$videos = array();
	if ($rs){
		while ($row = mysqli_fetch_assoc($rs)){
				$pts  = explode('_', $row['meta_key']);
				$id = $pts[1];
				$attr= str_replace("videos_" . $id . "_", "", $row['meta_key']);
				$videos[$id][$attr] = $row['meta_value'];
		}
		ksort($videos);
		$videoOutput = array();
		foreach($videos as $video){
			$videoOutput[$video['video_select']][] = $video;
		}
	}
	return $videoOutput;
}

$videos = getVideos();
echo json_encode($videos, 1);





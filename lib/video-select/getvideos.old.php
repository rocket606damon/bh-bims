<?php

define ('WP_LOADFILE_DIR', '../../../../../');
define( 'WP_USE_THEMES', false );
require_once( WP_LOADFILE_DIR . 'wp-load.php' );

$g_videos = array();
$filterColumns = array(
	'video_index',
	//'video_url',
	//'video_title',
	//'video_publish_date',
	//'video_description',
	//'video_type',
	//'youtube',
	//'vimeo',
	'video_select'
);

function wp_getVideos($toggle){
	global $g_videos;
	global $wpdb;
		$rows = $wpdb->get_results("SELECT post_id FROM " . $wpdb->postmeta . " " .
		"WHERE meta_key LIKE 'videos_%select' " .
		"AND meta_value='$toggle'", OBJECT
	);
		foreach($rows as $r){
				$videos = $wpdb->get_results("SELECT * FROM ".$wpdb->postmeta." " .
			"WHERE post_id='" . $r->post_id . "' " .
			"AND meta_key LIKE 'videos_%'", OBJECT
		);
		foreach($videos as $video){
			index_video($video);
				}
		}
	return filter_videos($toggle);
}

function index_video($video){
	global $filterColumns;
	global $g_videos;

	$kp = explode('_', $video->meta_key);
	$index = $kp[1];
	$prefix = "videos_" . $index . "_";
	$short_key = str_replace($prefix, '', $video->meta_key);
	if ($short_key == 'video_select' || in_array($short_key, $filterColumns)){
		$g_videos[$index][$short_key] = $video->meta_value;
		$g_videos[$index]['video_index'] = $index +1;
	}
}

function filter_videos($toggle){
	global $g_videos;

	$finalVideos = array();
	foreach($g_videos as $id => $video){
		if ($video['video_select'] == $toggle){
			unset($video['video_select']);
			$finalVideos[] = $video;
		}
	}
	return array('toggle' => $toggle, 'videos' => $finalVideos);
}

$key = 'toggle';
if (isset($_GET[$key])){
	$value = $_GET[$key];
	$return = wp_getVideos($value);
	if (!is_array($return)){
		echo $return;
	}else{
		$videos = $return;
		//print_r($videos);
		echo (json_encode($videos));
	}
}

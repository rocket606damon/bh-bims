<?php

define ('WP_LOADFILE_DIR', '../../../../../');
define( 'WP_USE_THEMES', false );
require_once( WP_LOADFILE_DIR . 'wp-load.php' );

function wp_getVideos(){
	global $g_videos;
	global $wpdb;
	$g_videos = $wpdb->get_results("SELECT * FROM " . $wpdb->postmeta . " WHERE meta_key LIKE 'videos_%select' ", OBJECT);
}

wp_getVideos();
echo (json_encode($g_videos));

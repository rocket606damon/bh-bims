<?php

define ('WP_LOADFILE_DIR', '../../../../../');
define( 'WP_USE_THEMES', false );
require_once( WP_LOADFILE_DIR . 'wp-load.php' );

$g_citations = array();
$filterColumns = array(
	'video_index',
	//'video_url',
	//'video_title',
	//'video_publish_date',
	//'video_description',
	//'video_type',
	//'youtube',
	//'vimeo',
	'video_select'
);

function wp_getCitations($toggle){
	global $g_citations;
	$citations = json_decode(file_get_contents("papers.json"), 1);
	foreach($citations as $citation){
		index_citation($citation);
	}
	return filter_citation($toggle);
}

function index_citation($citaton){
	global $filterColumns;
	global $g_citations;

	$index = count($g_citations);
	$prefix = "videos_" . $index . "_";
	$short_key = str_replace($prefix, '', $video->meta_key);
	if ($short_key == 'video_select' || in_array($short_key, $filterColumns)){
		$g_videos[$index] = $citation;
		$g_videos[$index]['citation_index'] = $index +1;
	}
}

$key = 'toggle';
if (isset($_GET[$key])){
	$value = $_GET[$key];
	$return = wp_getVideos($value);
	if (!is_array($return)){
		echo $return;
	}else{
		$videos = $return;
		//print_r($videos);
		echo (json_encode($videos));
	}
}

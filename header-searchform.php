<!--<form class="search header_search clearfix" method="get" id="searchform" action="<?php echo esc_url(home_url('/')); ?>">
	<input class="col-md-12 search_text" id="appendedInputButton" placeholder="<?php _e( 'Search...', 'asalah' ); ?>" type="text" name="s">

	<?php if (asalah_option('asalah_search_content') == 'posts'): ?>
	<input type="hidden" name="post_type" value="post" />
	<?php elseif(asalah_option('asalah_search_content') == 'projects'): ?>
	<input type="hidden" name="post_type" value="project" />
	<?php endif; ?>
	
	<i class="fa fa-search"><input type="submit" id="searchsubmit" value="" /></i>
</form>-->

<form id="searchform" method="get" action="<?php echo esc_url(home_url('/')); ?>">
	<div class="input-group col-md-12">
		<input type="text" class="form-control input-lg" placeholder="<?php _e( 'Search...', 'asalah' ); ?>" name="s">
		<?php if (asalah_option('asalah_search_content') == 'posts'): ?>
		<input type="hidden" name="post_type" value="post" />
		<?php elseif(asalah_option('asalah_search_content') == 'projects'): ?>
		<input type="hidden" name="post_type" value="project" />
		<?php endif; ?>
		<span class="input-group-btn">
			<button class="btn btn-info btn-lg" type="submit">
				<i class="fa fa-search"></i>
			</button>
		</span>
	</div>

</form>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<meta http-equiv="Content-type" content="text/html; charset=UTF-8">-->
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title><?php wp_title('|', true, 'right'); ?>
    </title>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <!-- start favicon and apple icons -->
    <?php global $asalah_data; ?>
    <?php if (asalah_option("asalah_fav_url")): ?>
        <link rel="shortcut icon" href="<?php echo asalah_option("asalah_fav_url"); ?>" title="Favicon" />
    <?php endif; ?>
    <?php if (asalah_option("asalah_apple_57")): ?>
        <link rel="apple-touch-icon-precomposed" href="<?php echo asalah_option("asalah_apple_57"); ?>" />
    <?php endif; ?>
    <?php if (asalah_option("asalah_apple_72")): ?>
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo asalah_option("asalah_apple_72"); ?>" />
    <?php endif; ?>
    <?php if (asalah_option("asalah_apple_114")): ?>
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo asalah_option("asalah_apple_114"); ?>" />
    <?php endif; ?>
    <?php if (asalah_option("asalah_apple_144")): ?>
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo asalah_option("asalah_apple_144"); ?>" />
    <?php endif; ?>
    <!-- end favicons and apple icons -->
	<style type="text/css">
		#scrollToTopButton {
			display: none; /* Hidden by default */
			position: fixed; /* Fixed/sticky position */
			bottom: 20px; /* Place the button at the bottom of the page */
			right: 30px; /* Place the button 30px from the right */
			z-index: 1099; /* Make sure it does not overlap */
			border: none; /* Remove borders */
			outline: none; /* Remove outline */
			background-color: red; /* Set a background color */
			color: white; /* Text color */
			cursor: pointer; /* Add a mouse pointer on hover */
			padding: 15px; /* Some padding */
			border-radius: 10px; /* Rounded corners */
		}
		#scrollToTopButton:hover {
			background-color: #555; /* Add a dark-grey background on hover */
		}
		#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
		/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
		   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
	</style>
	<?php wp_head(); ?>
</head>

<?php
$body_classes_array = array(asalah_body_class());
$header_position = "top";
$header_class =  "";
$header_container = "container";

$body_classes_array[] = "resize"; /* Added in */

if (asalah_post_option('asalah_onepage_scroll') == 'yes') {
    wp_enqueue_style('asalah_onepagescroll');
    wp_enqueue_script('asalah_easing');
    wp_enqueue_script('asalah_slimscroll');
    wp_enqueue_script('asalah_onepagescroll');
    $body_classes_array[] = "one_page_scroll";
}

if(asalah_cross_option("asalah_header_style") == "black_overlay") {
    $header_class = "overlay_header";
    $header_class .= ' '. asalah_cross_option("asalah_header_style") . '_header';
}elseif(asalah_cross_option("asalah_header_style") == "white_overlay") {
    $header_class = "overlay_header";
    $header_class .= ' '. asalah_cross_option("asalah_header_style") . '_header';
}else {
    $header_class .= ' '. asalah_cross_option("asalah_header_style") . '_header';
}

if (asalah_cross_option("asalah_sticky_header") == "yes") {
    $header_class .= " fixed_header";
}else{
    $body_classes_array[] = " no_sticky_header";
    $body_classes_array[] = " fixed_header_mobile_disabled";
    $body_classes_array[] = " fixed_header_tablet_disabled";
}

if (asalah_cross_option("asalah_sticky_header_mobile_disable") == "yes") {
    $body_classes_array[] = " fixed_header_mobile_disabled";
}

if (asalah_cross_option("asalah_sticky_header_tablet_disable") == "yes") {
    $body_classes_array[] = " fixed_header_tablet_disabled";
}

if (asalah_cross_option("asalah_header_style") == "fixed_left") {
    $header_class = "fixed_left fixed_aside";
    $body_classes_array[] = "side_header";
    $header_position = "side";
    echo "<style>html {margin-left: 240px;}</style>";
}elseif(asalah_cross_option("asalah_header_style") == "fixed_right") {
    $header_class = "fixed_right fixed_aside";
    $body_classes_array[] = "side_header";
    $header_position = "side";
    echo "<style>html {margin-right: 240px;}</style>";
}



if (asalah_cross_option("asalah_header_items_position")) {
    $header_class .= ' '. asalah_cross_option("asalah_header_items_position") . '_position_header';
}

if (asalah_cross_option("asalah_dark_header") == "yes") {
    $header_class .= ' dark_header';
}

if (asalah_cross_option("asalah_top_header_scheme") == "dark") {
    $header_class .= ' dark_top_header';
}

if (asalah_cross_option("asalah_transparent_header") == "yes") {
    $header_class .= ' transparent_header';
}

if (asalah_cross_option('asalah_header_wrapper') == "container") {

     $header_container = "container";
}



?>
<body id="all_site" <?php body_class($body_classes_array); ?>>
  <?php if (asalah_cross_option('asalah_header_code')) { echo asalah_cross_option('asalah_header_code'); } ?>
    <!-- start facebook sdk -->
    <?php if (asalah_option('asalah_use_sdk') && asalah_option('asalah_fb_id')): ?>
        <!-- Load facebook SDK -->
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=<?php echo asalah_option('asalah_fb_id'); ?>";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <!-- End Load facebook SDK -->
    <?php endif; ?>

    <?php
    ///////////////////////////////////////////////////////
    //////////////// add cross options here ///////////////
    ///////////////////////////////////////////////////////


    // define show header elemnts from post options and theme options
    $show_search = $show_contact = $show_social = $show_cart = $show_language = false;

    if (asalah_cross_option("asalah_header_contact") == "show") {
        $show_contact = true;
    }

    if (asalah_cross_option("asalah_header_social") == "show") {
        $show_social = true;
    }

    if (asalah_cross_option("asalah_header_search") == "show") {
        $show_search = true;
    }

    if (asalah_cross_option("asalah_header_cart") == "show") {
        $show_cart = true;
    }

    // if (asalah_cross_option("asalah_header_language") == "show") {
    //     $show_language = true;
    // }

    if (!$show_search) {
        $header_class .= " no_header_search";
    }

    ////////////////////////////////////////////////////////////////
    //////////////// start search contact and social ///////////////
    ////////////////////////////////////////////////////////////////

    ?>
    <!-- Start Mobile Menu Button -->
    <?php if ( has_nav_menu( 'mainmenu' ) ) { ?>
        <!-- Start Mobile Navigation -->
        <nav class="slide_menu_list_wrapper">
        <?php
        if (asalah_post_option('asalah_custom_menu_header') && asalah_post_option('asalah_custom_menu_header') != 'none') {
          wp_nav_menu(array(
              'container' => 'div',
              'container_class' => '',
              'container_id' => 'slide_menu_list',
              'menu' => asalah_cross_option('asalah_custom_menu_header'),
              'menu_class' => 'slide_menu_list',
              'fallback_cb' => '',
              'walker' => new wp_bootstrap_navwalker(),
              ));
        } else {
          wp_nav_menu(array(
              'container' => 'div',
              'container_class' => '',
              'container_id' => 'slide_menu_list',
              'theme_location' => 'mainmenu',
              'menu_class' => 'slide_menu_list',
              'fallback_cb' => '',
              'walker' => new wp_bootstrap_navwalker(),
              ));
        }
            ?>
        </nav>
    <?php } ?>
    <!-- end mobile menu -->

    <div class="all_site_wrapper canvas-off">
    <header id="site_header" class="site_header <?php echo esc_attr($header_class); ?> unsticky_header">
        <div class="top-bar">
          <div class="container">
            <div class="social">
              <script type="text/javascript">
              function googleTranslateElementInit() {
                new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
              }
              </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
              <div id="google-translator"><span class="up_bold">Translate:</span><a href="#" title="Translate Page" class="btn-translate"><img src="<?php echo get_template_directory_uri(); ?>/images/language.png" alt="translate" /></a><div id="google_translate_element"></div></div>
              <div class="resize resize-header clearfix">
                <p><span class="up_bold">Text Size: </span><a id="decrease-font" href="#">A- </a><a id="increase-font" href="#">A+ </a> </p>
              </div>
            </div>
            <div class="top-right">
              <ul class="nav-utility clearfix">
                <li><a href="#" data-toggle="modal" data-target="#newsletter-signup"><i class="fa fa-envelope" aria-hidden="true"></i>Join Mailing List</a></li>
                <li class="social-top">
                  <a class="social-thumbs fa fa-facebook" href="https://www.facebook.com/bhbims/" target="_blank"></a>
                  <a class="social-thumbs fa fa-twitter" href="https://twitter.com/bostonburn_ms" target="_blank"></a>
                  <a class="social-thumbs fa fa-youtube" href="https://www.youtube.com/user/BostonHarvardBIMS" target="_blank"></a>
                </li>
              </ul>
            </div>
          </div>
        </div>

        <!-- Reading progress bar -->
        <?php if (asalah_option('asalah_reading_progress') && is_single()) : ?>
          <progress value="0" id="reading_progress">
            <div class="reading-progress-bar">
                <span style="width:0;">Progress: 0%</span>
            </div>
          </progress>
        <?php endif; ?>
        <!-- start top header in case of top header position -->
        <?php if ($header_position == "top"): ?>
        <?php if ( $show_contact || $show_social ) : ?>
            <!-- start header_top container -->
            <div class="header_top">
				<a href="#top" id="toTop"></a>
                <div class="<?php echo esc_attr($header_container); ?>">
                    <!-- start header info -->
                    <div class="header_info clearfix">

                    <!-- start contact info -->
                    <?php if ($show_contact): ?>
                        <!-- start contact info -->
                        <div class="contact_info pull-left">
                           <?php if (asalah_option("asalah_header_mail")): ?>
                            <span class="contact_info_item email_address"><i class="fa fa-envelope"></i> <a href="mailto:<?php echo asalah_option("asalah_header_mail"); ?>"><?php echo asalah_option("asalah_header_mail"); ?></a></span>
                        <?php endif; ?>

                        <?php if (asalah_option("asalah_header_phone")): ?>
                            <span class="contact_info_item phone_number"><i class="fa fa-phone"></i> <a href="callto:<?php echo asalah_option("asalah_header_phone"); ?>"><?php echo asalah_option("asalah_header_phone"); ?></a>
                            </span>
                        <?php endif; ?>
                    </div>
                    <?php endif; ?>
                    <!-- end contact info -->

                    <?php if ($show_language): ?>
                        <div class="asalah_language_switcher pull-right ">
                            <?php do_action('icl_language_selector'); ?>
                        </div>
                    <?php endif; ?>

                    <!-- start header social icons -->
                    <?php if ($show_social): ?>
                        <!-- start social icons -->
                        <div class="header_social pull-right">
                            <?php echo asalah_social_icons_list(); ?>
                        </div>
                        <!-- end social icons -->
                    <?php endif; ?>
                    <!-- end header social icons -->

                    </div><!-- end header info -->
                </div> <!-- end container -->
            </div> <!-- end header top -->
        <?php endif; ?>
        <?php endif; ?>
        <!-- end header_top container -->


    <div class="header_below container-fluid">

        <div class="<?php echo esc_attr($header_container); ?>">
            <div class="row">
                <div class="col-md-12">
                  <div class="first_header_wrapper tranparent_header clearfix">
                        <!-- <div class="header_overlay"></div> -->
                        <?php
                        // get logo options from theme options or post options
                        $logo_url = $logo_retina_url = $logo_url_sticky = $logo_retina_url_sticky = '';
                        $logo_w = $logo_h = $logo_w_sticky = $logo_h_sticky = 'auto';

                        //////////////////////////////////////////////////////////////
                        //////////////// start logo url and dimentions ///////////////
                        //////////////////////////////////////////////////////////////

                        if (asalah_cross_option("asalah_logo_url")) {
                            $logo_url = asalah_cross_option("asalah_logo_url");
                            if (asalah_cross_option("asalah_logo_url_retina")) {
                                $logo_retina_url = asalah_cross_option("asalah_logo_url_retina");
                            }
                            if (asalah_post_option("asalah_logo_url") && ( !asalah_post_option("asalah_logo_url_retina") || asalah_post_option("asalah_logo_url_retina") == '')  ) {
                                $logo_retina_url = null;
                            }
                            if (asalah_cross_option("asalah_logo_url_w")) {
                                $logo_w = asalah_cross_option("asalah_logo_url_w");
                            }
                            if (asalah_cross_option("asalah_logo_url_h")) {
                                $logo_h = asalah_cross_option("asalah_logo_url_h");
                            }
                        }


                        if (asalah_cross_option("asalah_sticky_logo_url")) {
                            if (asalah_post_option("asalah_default_option_sticky_logo") == "yes") {
                                $logo_url_sticky = asalah_option("asalah_sticky_logo_url");
                                $logo_retina_url_sticky = asalah_option("asalah_sticky_logo_url_retina");
                            }else{
                                $logo_url_sticky = asalah_post_option("asalah_sticky_logo_url");
                                $logo_retina_url_sticky = asalah_post_option("asalah_sticky_logo_url_retina");
                            }
                        }

                        if (asalah_cross_option("asalah_sticky_logo_width")) {
                            $logo_w_sticky = asalah_cross_option("asalah_sticky_logo_width");
                        }
                        if (asalah_cross_option("asalah_sticky_logo_height")) {
                            $logo_h_sticky = asalah_cross_option("asalah_sticky_logo_height");
                        }



                        // start style for logo and headers items from post options
                        $header_style_output = "";

                        if (asalah_post_option('asalah_logo_url_h') || asalah_post_option('asalah_logo_url_w')) {
                            $header_style_output .= ".logo img {";
                            if (asalah_post_option('asalah_logo_url_w')) {
                                $header_style_output .= "width:" . asalah_post_option('asalah_logo_url_w') . "px;";

                            }else{
                                $header_style_output .= "width: auto;";
                            }

                            if (asalah_post_option('asalah_logo_url_h')) {
                                $header_style_output .= "height:" . asalah_post_option('asalah_logo_url_h') . "px;";
                            }else{
                                $header_style_output .= "height: auto;";
                            }

                            $header_style_output .= "}";
                        }

                        if (asalah_cross_option('asalah_sticky_logo_height') || asalah_cross_option('asalah_sticky_logo_width')) {
                            $header_style_output .= ".sticky_logo img {";
                            if (asalah_post_option('asalah_sticky_logo_width')) {
                                $header_style_output .= "width:" . asalah_post_option('asalah_sticky_logo_width') . "px;";

                            }else{
                                $header_style_output .= "width: auto;";
                            }

                            if (asalah_post_option('asalah_sticky_logo_height')) {
                                $header_style_output .= "height:" . asalah_post_option('asalah_sticky_logo_height') . "px;";
                            }else{
                                $header_style_output .= "height: auto;";
                            }
                            $header_style_output .= "}";

                            if (!asalah_cross_option("asalah_sticky_logo_url")
                                || ( !asalah_post_option(".asalah_sticky_logo_url") && asalah_post_option("asalah_default_option_sticky_logo") != "yes" ) ) {
                                $header_style_output .= ".sticky_header .logo img {";
                                    if ($logo_w_sticky != 'auto') {
                                        $header_style_output .= "width:" . $logo_w_sticky . "px;";
                                    }
                                    if ($logo_h_sticky != 'auto') {
                                        $header_style_output .= "height:" . $logo_h_sticky . "px;";
                                    }
                                $header_style_output .= "}";

                                if (asalah_cross_option("asalah_logo_margin_top")) {
                                    $header_style_output .= ".sticky_header .logo {";
                                        $header_style_output .= "margin-top:" . asalah_cross_option('asalah_sticky_logo_margin_top') . "px;";
                                    $header_style_output .= "}";
                                }
                            }

                        }


                        if (asalah_post_option('asalah_header_padding_top')) {
                            $header_style_output .= ".header_below {";
                                $header_style_output .= "padding-top:" . asalah_post_option('asalah_header_padding_top') . "px;";
                            $header_style_output .= "}";
                        }

                        if (asalah_post_option('asalah_header_padding_bottom')) {
                            $header_style_output .= ".header_below {";
                                $header_style_output .= "padding-bottom:" . asalah_post_option('asalah_header_padding_bottom') . "px;";
                            $header_style_output .= "}";
                        }

                        if (asalah_post_option('asalah_sticky_padding_top')) {
                            $header_style_output .= ".site_header.fixed_header.sticky_header .header_below {";
                                $header_style_output .= "padding-top:" . asalah_post_option('asalah_sticky_padding_top') . "px;";
                            $header_style_output .= "}";
                        }

                        if (asalah_post_option('asalah_sticky_padding_bottom')) {
                            $header_style_output .= ".site_header.fixed_header.sticky_header .header_below {";
                                $header_style_output .= "padding-bottom:" . asalah_post_option('asalah_sticky_padding_bottom') . "px;";
                            $header_style_output .= "}";
                        }

                        if (asalah_post_option("asalah_logo_margin_top")) {
                            $header_style_output .= ".logo {";
                                $header_style_output .= "margin-top:" . asalah_post_option('asalah_logo_margin_top') . "px;";
                            $header_style_output .= "}";
                        }

                        if (asalah_post_option("asalah_sticky_logo_margin_top")) {
                            $header_style_output .= ".sticky_logo {";
                                $header_style_output .= "margin-top:" . asalah_post_option('asalah_sticky_logo_margin_top') . "px;";
                            $header_style_output .= "}";
                        }

                        if (asalah_post_option("asalah_menu_margin_top")) {
                            $header_style_output .= ".main_navbar {";
                                $header_style_output .= "margin-top:" . asalah_post_option('asalah_menu_margin_top') . "px;";
                            $header_style_output .= "}";
                        }

                        if (asalah_post_option('asalah_sticky_menu_margin_top')) {
                            $header_style_output .= ".sticky_header .main_navbar {";
                                $header_style_output .= "margin-top:" . asalah_post_option('asalah_sticky_menu_margin_top') . "px;";
                            $header_style_output .= "}";
                        }

                        if (asalah_post_option("asalah_header_buttons_margin_top")) {
                            $header_style_output .= ".header_button {";
                                $header_style_output .= "margin-top:" . asalah_post_option('asalah_header_buttons_margin_top') . "px;";
                            $header_style_output .= "}";
                        }

                        if (asalah_post_option('asalah_sticky_header_buttons_margin_top')) {
                            $header_style_output .= ".sticky_header .header_button {";
                                $header_style_output .= "margin-top:" . asalah_post_option('asalah_sticky_header_buttons_margin_top') . "px;";
                            $header_style_output .= "}";
                        }

                        if (asalah_post_option('asalah_menu_items_padding_top')) {
                            $header_style_output .= ".first_header_wrapper .navbar .nav>li>a {";
                                $header_style_output .= "padding-top:" . asalah_post_option('asalah_menu_items_padding_top') . "px;";
                            $header_style_output .= "}";
                        }

                        if (asalah_post_option('asalah_menu_items_padding_bottom')) {
                            $header_style_output .= ".first_header_wrapper .navbar .nav>li>a {";
                                $header_style_output .= "padding-bottom:" . asalah_post_option('asalah_menu_items_padding_bottom') . "px;";
                            $header_style_output .= "}";
                        }

                        if (asalah_post_option('asalah_sticky_menu_items_padding_top')) {
                            $header_style_output .= ".site_header.fixed_header.sticky_header .first_header_wrapper .navbar .nav>li>a {";
                                $header_style_output .= "padding-top:" . asalah_post_option('asalah_sticky_menu_items_padding_top') . "px;";
                            $header_style_output .= "}";
                        }

                        if (asalah_post_option('asalah_sticky_menu_items_padding_bottom')) {
                            $header_style_output .= ".site_header.fixed_header.sticky_header .first_header_wrapper .navbar .nav>li>a {";
                                $header_style_output .= "padding-bottom:" . asalah_post_option('asalah_sticky_menu_items_padding_bottom') . "px;";
                            $header_style_output .= "}";
                        }

                        echo "<style scoped type='text/css' media='all'>";
                            echo balanceTags($header_style_output);
                        echo "</style>";
                        ?>
                        <!-- start site logo -->
                        <?php if ($logo_url): ?>
                            <?php
                            $logo_class = "";
                            if (!$logo_retina_url) {
                                $logo_class = " no_retina_logo";
                            }
                            ?>
                            <div class="logo">
                                <a class="default_logo <?php echo esc_attr($logo_class); ?>" href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"><img style="width:<?php echo esc_attr($logo_w); ?>;height:<?php echo esc_attr($logo_h); ?>" src="<?php echo esc_url($logo_url); ?>" alt="<?php bloginfo('name'); ?>"><strong class="hidden"><?php bloginfo('name'); ?></strong></a>

                                <!-- start retina logo -->
                                <?php if ($logo_retina_url) { ?>
                                <a class="retina_logo" href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"><img style="width:<?php echo esc_attr($logo_w); ?>;height:<?php echo esc_attr($logo_h); ?>" src="<?php echo esc_url($logo_retina_url); ?>" alt="<?php bloginfo('name'); ?>"><strong class="hidden"><?php bloginfo('name'); ?></strong></a>
                                <?php } ?>
                                <!-- end retina logo -->
                            </div>
                        <?php else: ?>

                            <!-- Text logo if no logo uploaded in option panel -->
                            <div class="logo"><h1><a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php echo get_bloginfo('name'); ?></a></h1></div>
                        <?php endif; ?>

                        <?php if ($logo_url_sticky): ?>
                            <?php
                            $sticky_logo_class = "";
                            if (!$logo_retina_url_sticky) {
                                $sticky_logo_class = " no_retina_logo";
                            }
                            ?>
                            <div class="sticky_logo">
                                <a class="default_logo <?php echo esc_attr($sticky_logo_class); ?>" href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"><img style="width:<?php echo esc_attr($logo_w_sticky); ?>;height:<?php echo esc_attr($logo_h_sticky); ?>" src="<?php echo esc_url($logo_url_sticky); ?>" alt="<?php bloginfo('name'); ?>"><strong class="hidden"><?php bloginfo('name'); ?></strong></a>

                                <!-- start retina logo -->
                                <?php if ($logo_retina_url_sticky) { ?>
                                <a class="retina_logo" href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"><img style="width:<?php echo esc_attr($logo_w_sticky); ?>;height:<?php echo esc_attr($logo_h_sticky); ?>" src="<?php echo esc_url($logo_retina_url_sticky); ?>" alt="<?php bloginfo('name'); ?>"><strong class="hidden"><?php bloginfo('name'); ?></strong></a>
                                <?php } ?>
                                <!-- end retina logo -->
                            </div>
                        <?php endif; ?>
                        <!-- end site logo -->

                    <div class="header_below_control_wrapper">
                      <!-- start search box -->
                      <div class="search pull-right ">
                          <div class="sub-search"><?php get_template_part( 'header', 'searchform' ); ?></div>
                        <!-- end search box -->
                            <!-- Start Search Button -->
                            <?php if ( ($show_search || $show_cart ) && $header_position == "top"): ?>

                                <div class="header_button pull-right">
                                   <!-- <a href="#" class="button button-green icon-cart">Buy Now</a> -->



                                <?php if ($show_search): ?>

                                    <?php
                                    $search_style_class = " simple_search";
                                    if (asalah_cross_option("asalah_search_style")) {
                                        $search_style_class = " ".asalah_cross_option("asalah_search_style")."_search";
                                    }
                                    ?>
                                   <!-- start go to top -->
                                   <div id="gototop" title="Scroll To Top" class="gototop pull-right">
                                    <i class="fa fa-chevron-up"></i>
                                   </div>
                                   <!-- end go to top -->



                                <?php endif; ?>

                                <?php if ($show_cart): ?>
                                   <!-- start cart button -->
                                   <?php global $woocommerce; ?>
                                   <div class="cart pull-right ">
                                       <?php do_action('header_checkout'); ?>
                                   </div>
                                   <!-- end cart button -->
                                <?php endif; ?>

                                </div>



                            <?php endif; ?>
                            <!-- End Search Button -->

                            <!-- Start Navigation -->
                            <?php if (asalah_cross_option("asalah_menu_style") == "full"): ?>
                                <!-- ////////////////////////////////////////// -->
                                <!-- ///////////// Full Width Menu //////////// -->
                                <!-- ////////////////////////////////////////// -->
                                <section class="main_navbar fullwidth_navbar_button">
                                    <button id="trigger-overlay" class="fa fa-align-justify overlay_button" type="button"></button>
                                </section>

                                <div class="overlay overlay-hugeinc">
                                    <button type="button" class="overlay-close"><i class="fa fa-times"></i></button>
                                    <?php
                                    if (asalah_post_option('asalah_custom_menu_header') && asalah_post_option('asalah_custom_menu_header') != 'none') {
                                      wp_nav_menu(array(
                                          'container' => 'div',
                                          'container_class' => '',
                                          'menu' => asalah_cross_option('asalah_custom_menu_header'),
                                          'menu_class' => 'full_width_menu',
                                          'fallback_cb' => '',
                                          'walker' => new wp_bootstrap_navwalker(),
                                          ));
                                    } else {
                                    wp_nav_menu(array(
                                        'container' => 'nav',
                                        'container_class' => '',
                                        'theme_location' => 'mainmenu',
                                        'menu_class' => 'full_width_menu',
                                        'fallback_cb' => '',
                                        'walker' => new wp_bootstrap_navwalker(),
                                        ));
                                      }
                                    ?>
                                </div>
                            <?php else: ?>

                                <?php
                                $main_menu_class = "default_menu_style";
                                if (asalah_cross_option("asalah_menu_style")) {
                                    $main_menu_class = asalah_cross_option("asalah_menu_style") . "_menu_style";
                                }
                                ?>

                                <!-- ////////////////////////////////////////// -->
                                <!-- //////////// Default Main Menu /////////// -->
                                <!-- ////////////////////////////////////////// -->
                                <div class="desktop_menu">
                                    <nav class="col-md- visible-desktop col-md- navbar main_navbar pull-right <?php echo esc_attr($main_menu_class); ?>">

                                    <?php
                                    if (asalah_post_option('asalah_custom_menu_header') && asalah_post_option('asalah_custom_menu_header') != 'none') {
                                      wp_nav_menu(array(
                                          'container' => 'div',
                                          'container_class' => 'main_nav',
                                          'menu' => asalah_cross_option('asalah_custom_menu_header'),
                                          'menu_class' => 'nav navbar-nav',
                                          'fallback_cb' => '',
                                          'walker' => new wp_bootstrap_navwalker(),
                                          ));
                                    } else {
                                    wp_nav_menu(array(
                                        'container' => 'div',
                                        'container_class' => 'main_nav',
                                        'theme_location' => 'mainmenu',
                                        'menu_class' => 'nav navbar-nav',
                                        'fallback_cb' => '',
                                        'walker' => new wp_bootstrap_navwalker(),
                                        ));
                                      }
                                    ?>
                                    </nav>
                                </div>
                                <!-- End Navigation -->

                                <!-- Start Mobile Menu Button -->
                                <?php if ( has_nav_menu( 'mainmenu' ) ) { ?>
                                <div class="mobile_menu">
                                    <div class="mobile_menu_button">
                                        <a class="mobile_menu_target" href="#slide_menu_list"><i id="showLeftPush" class="fa fa-align-justify"></i></a>
                                    </div>
                                    <!-- End Mobile Menu Button -->
                                </div>
                                <?php } ?>
                                <!-- end mobile menu -->
                            <?php endif; ?>
                            <!-- End Mobile Navigation -->
                          </div> <!-- end search & pull right -->
                        </div> <!-- end header_below_control_wrapper -->

                        </div> <!-- end first_header_wrapper -->
                    </div> <!-- end col-md-12 -->

                </div> <!-- end row -->
            </div> <!-- end container -->
        </div> <!-- end container-fluid -->

        <!-- //////////////////////////////////////////////// -->
        <!-- ///////////// Other Navigation Test //////////// -->
        <!-- //////////////////////////////////////////////// -->

        <div class="nav-main navbar navbar-default navbar-static-top">
        <div class="container">
          <div class="row">
            <div class="navbar-header">
            <!-- <p class="navbar-text collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" role="button">Menu</p> -->
              <a class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </a>
            </div>
            <div id="nav-wrapper">
            <?php
                wp_nav_menu( array(
                'menu'              => 'header',
                'theme_location'    => 'header',
                'depth'             => 2,
                'container'         => 'nav',
                'container_class'   => 'collapse navbar-collapse nav-justified',
                'menu_class'        => 'nav nav-justified',
                'container_id'      => 'menu-primary-navigation',
                'fallback_cb'       => 'custom_navwalker::fallback',
                'walker'            => new custom_navwalker())
                );
            ?>
            </div>
            <!--/.nav-collapse -->

            </div>
          </div>
        </div>

        <!-- start header top in case of side header -->
        <?php if ($header_position == "side"): ?>
            <?php if ( $show_contact || $show_social || $show_search || $show_language ) : ?>
                <!-- start header_top container -->
                <div class="header_top">
                    <div class="container">
                        <!-- start header info -->
                        <div class="header_info clearfix">
                        <!-- start contact info -->
                        <?php if ($show_contact): ?>
                            <!-- start contact info -->
                            <div class="contact_info pull-left">
                            <?php if (asalah_option("asalah_header_mail")): ?>
                                <span class="contact_info_item email_address"><i class="fa fa-envelope"></i> <a href="mailto:<?php echo asalah_option("asalah_header_mail"); ?>"><?php echo asalah_option("asalah_header_mail"); ?></a></span>
                            <?php endif; ?>

                            <?php if (asalah_option("asalah_header_phone")): ?>
                                <span class="contact_info_item phone_number"><i class="fa fa-phone"></i> <a href="callto:<?php echo asalah_option("asalah_header_phone"); ?>"><?php echo asalah_option("asalah_header_phone"); ?></a>
                                </span>
                            <?php endif; ?>
                            </div>
                        <?php endif; ?>
                        <!-- end contact info -->

                        <!-- start header social icons -->
                        <?php if ($show_social): ?>
                            <!-- start social icons -->
                            <div class="header_social pull-right">
                                <?php echo asalah_social_icons_list(); ?>
                            </div>
                            <!-- end social icons -->
                        <?php endif; ?>
                        <!-- end header social icons -->

                        <?php if ($show_language): ?>
                            <div class="asalah_language_switcher pull-right ">
                                <?php do_action('icl_language_selector'); ?>
                            </div>
                        <?php endif; ?>


                        <!-- Start Search Button -->
                        <?php if ($show_search): ?>
                            <?php
                            $search_style_class = " simple_search";
                            if (asalah_cross_option("asalah_search_style")) {
                                $search_style_class = " ".asalah_cross_option("asalah_search_style")."_search";
                            }
                            ?>
                        <!-- Start Search Button -->
                        <div class="header_button top_header_button">
                            <!-- <a href="#" class="button button-green icon-cart">Buy Now</a> -->
                            <!-- start search box -->
                            <div class="search <?php echo esc_attr($search_style_class); ?>">
                                <?php get_template_part( 'header', 'searchform' ); ?>
                            </div>
                            <!-- end search box -->
                        </div>
                        <?php endif; ?>
                        <!-- End Search Button -->

                        </div><!-- end header info -->
                    </div> <!-- end container -->
                </div> <!-- end header top -->
            <?php endif; ?>
        <?php endif; ?>
        <!-- end header_top container -->
    </header>

    <div class="fixed_header_height <?php echo esc_attr($header_class); ?> unsticky_header"></div>
    <!-- End Site Header -->

    <?php

    if ((is_home()) && (asalah_cross_option('asalah_header_ads_enabled'))) {
    /* Google Ads Theme Options */
    $devices = array('all', 'desktop', 'tablet', 'mobile');
    foreach ($devices as $device) {
      if (asalah_cross_option('asalah_header_ads_'.$device)) {
        echo asalah_insert_ad(asalah_cross_option('asalah_header_ads_'.$device), '10', '0', asalah_cross_option('asalah_header_ads_width_'.$device), asalah_cross_option('asalah_header_ads_height_'.$device), $device);
      }
    }
  }
    ?>

    <div class="modal" id="newsletter-signup" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
               <h3 class="modal-title" id="myModalLabel">BH-BIMS Newsletter Sign-Up</h3>
            </div>
            <div class="modal-body">
            <!-- Begin MailChimp Signup Form -->
                <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
                <div id="mc_embed_signup">
                <form action="//bh-bims.us11.list-manage.com/subscribe/post?u=aaba1f619216d015f22cdb26c&amp;id=c25b27c777" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">
                    <h2>Subscribe to our mailing list</h2>
                <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
                <div class="mc-field-group">
                    <label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
                </label>
                    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                </div>
                <div class="mc-field-group">
                    <label for="mce-FNAME">First Name </label>
                    <input type="text" value="" name="FNAME" class="" id="mce-FNAME">
                </div>
                <div class="mc-field-group">
                    <label for="mce-LNAME">Last Name </label>
                    <input type="text" value="" name="LNAME" class="" id="mce-LNAME">
                </div>
                    <div id="mce-responses" class="clear">
                        <div class="response" id="mce-error-response" style="display:none"></div>
                        <div class="response" id="mce-success-response" style="display:none"></div>
                    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_aaba1f619216d015f22cdb26c_c25b27c777" tabindex="-1" value=""></div>
                    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                    </div>
                </form>
                </div>
                <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
            <!--End mc_embed_signup-->
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-danger" data-dismiss="modal">Cancel</button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="asalah_site_content">